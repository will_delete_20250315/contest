package com.itrui.buyitbackend.pojo;

import lombok.Data;

@Data
public class MyAddress {
    private Integer addressId       ;
    private String  addressName     ;
    private String  addressPhone    ;
    private String  addressMain     ;
    private Integer userAccount     ;
    private String  addressDetail   ;
}
