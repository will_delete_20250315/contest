package com.itrui.buyitbackend.service.Impl;

import com.itrui.buyitbackend.mapper.UserMapper;
import com.itrui.buyitbackend.pojo.User;
import com.itrui.buyitbackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public User userLogin(Integer account) {
        return userMapper.userLogin(account);
    }

    @Override
    public boolean userRegister(User user) {
        return userMapper.userRegister(user) > 0;
    }

    @Override
    public boolean updataUserPasswprd(User user) {
        return userMapper.updataUserPasswprd(user) > 0;
    }

    @Override
    public List<User> getAllUSer() {
        return userMapper.getAllUSer();
    }

    @Override
    public User getUerByAccount(Integer account) {
        return userMapper.getUerByAccount(account);
    }

    @Override
    public User getUserById(Integer id) {
        return userMapper.getUserById(id);
    }


}
