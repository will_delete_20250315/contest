为期两周的培训，可以说收获甚微，也可以说收获巨大
说收获甚微呢是因为老师一直强调的要理解代码这是最重要最有价值的，但是对于代码我理解得还是非常少的
那收获巨大呢是因为它给了我极大的信心
我选这个培训的原因是因为我以后打算就业的职位是数字IC设计工程师，其中有个技能很重要，就是Verilog语言，
它和C语言非常像，但又不一样，我原来的打算是暑假重新学一遍C语言，结果软通培训正好和C语言相关，所以我就来了。
但是头两天的环境搭建就已经很人难受了，再到后来代码讲解，犹如天书一天，我基本都听不懂，所以就很难受，似乎一点收获没有，
然后我产生了摆烂的念头，但是我转念一想，如果我连着两周的培训都坚持不了，那我以后就业怎么办，每天995的工作，加上入职后要学的很多东西
所幸我最后坚持下来了，所以这也给了我极大的信心，让我去面对将来的就业
还有一个就是我们组原来的组长这周被公司召唤过去了，所以我们组群龙无首，面对大作业和答辩稍微有点乱
临危受命我当了新组长，过程很繁琐，我也是一直在不断跟自己说心态要平和，最后也是在答辩前做出来了
最后要感谢我们两位老师，他们真的非常耐心，同样的错误我犯了好几次老师都很和蔼的没有一丝不耐烦的给我讲解怎么去解决这个错误，
非常喜欢冷老师每次给我解决完错误后那句“这就阔以了噻”，这句话真的很哇塞的，最后真的非常感谢两位老师。
