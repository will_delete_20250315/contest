## Introduction to Speexdsp

Speexdsp is a speech processing library that goes along with the Speex codec.

## The main characteristics of Speexdsp

### Preprocessor
The preprocessor is designed to be used on the audio before running the encoder. The preprocessor provides three main functionalities:

- noise suppression
- automatic gain control (AGC)
- voice activity detection (VAD)

The denoiser can be used to reduce the amount of background noise present in the input signal. This provides higher quality
speech whether or not the denoised signal is encoded with Speex (or at all). However, when using the denoised signal with the codec, there is an additional benefit. Speech codecs in general (Speex included) tend to perform poorly on noisy input, which tends to amplify the noise. The denoiser greatly reduces this effect.

Automatic gain control (AGC) is a feature that deals with the fact that the recording volume may vary by a large amount between different setups. The AGC provides a way to adjust a signal to a reference volume. This is useful for voice over IP because it removes the need for manual adjustment of the microphone gain. A secondary advantage is that by setting the
microphone gain to a conservative (low) level, it is easier to avoid clipping.

The voice activity detector (VAD) provided by the preprocessor is more advanced than the one directly provided in the codec.

### Adaptive Jitter Buffer
When transmitting voice (or any content for that matter) over UDP or RTP , packet may be lost, arrive with different delay,or even out of order. The purpose of a jitter buffer is to reorder packets and buffer them long enough (but no longer than necessary) so they can be sent to be decoded.

### Acoustic Echo Canceller
In any hands-free communication system (Fig. 2.1), speech from the remote end is played in the local loudspeaker, propagates in the room and is captured by the microphone. If the audio captured from the microphone is sent directly to the remote end,then the remove user hears an echo of his voice. An acoustic echo canceller is designed to remove the acoustic echo before it is sent to the remote end. It is important to understand that the echo canceller is meant to improve the quality on the remote
end.

### Resampler
In some cases, it may be useful to convert audio from one sampling rate to another. There are many reasons for that. It can be for mixing streams that have different sampling rates, for supporting sampling rates that the soundcard doesn’t support, for transcoding, etc. That’s why there is now a resampler that is part of the Speex project. This resampler can be used to convert between any two arbitrary rates (the ratio must only be a rational number) and there is control over the quality/complexity tradeoff.

## Speech Processing API (libspeexdsp)
 
### Preprocessor

In order to use the Speex preprocessor, you first need to:
```C
#include <speex/speex_preprocess.h>
```

Then, a preprocessor state can be created as:
```C
SpeexPreprocessState *preprocess_state = speex_preprocess_state_init(frame_size,sampling_rate);
```

and it is recommended to use the same value for frame_size as is used by the encoder (20 ms).
For each input frame, you need to call:
```C
speex_preprocess_run(preprocess_state, audio_frame);
```

where audio_frame is used both as input and output. In cases where the output audio is not useful for a certain frame, it is possible to use instead:
```C
speex_preprocess_estimate_update(preprocess_state, audio_frame);
```

This call will update all the preprocessor internal state variables without computing the output audio, thus saving some CPU cycles.
The behaviour of the preprocessor can be changed using:
```C
speex_preprocess_ctl(preprocess_state, request, ptr);
```

which is used in the same way as the encoder and decoder equivalent. 
The preprocessor state can be destroyed using:

```C
speex_preprocess_state_destroy(preprocess_state);
```

### Echo Cancellation

The Speex library now includes an echo cancellation algorithm suitable for Acoustic Echo Cancellation (AEC). In order to use the echo canceller, you first need to
```C
#include <speex/speex_echo.h>
```
Then, an echo canceller state can be created by:
```C
SpeexEchoState *echo_state = speex_echo_state_init(frame_size, filter_length);
```

where `frame_size` is the amount of data (in samples) you want to process at once and `filter_length` is the length  (in samples) of the echo cancelling filter you want to use (also known as tail length). It is recommended to use a frame size in
the order of 20 ms (or equal to the codec frame size) and make sure it is easy to perform an FFT of that size (powers of two are
better than prime sizes). The recommended tail length is approximately the third of the room reverberation time. For example,
in a small room, reverberation time is in the order of 300 ms, so a tail length of 100 ms is a good choice (800 samples at 8000
Hz sampling rate).

Once the echo canceller state is created, audio can be processed by:
```C
speex_echo_cancellation(echo_state, input_frame, echo_frame, output_frame);
```
where `input_frame `is the audio as captured by the microphone, `echo_frame` is the signal that was played in the speaker (and needs to be removed) and `output_frame `is the signal with echo removed.

One important thing to keep in mind is the relationship between `input_frame` and `echo_frame`. It is important that, at any time, any echo that is present in the input has already been sent to the echo canceller as `echo_frame`. In other words,the echo canceller cannot remove a signal that it hasn’t yet received. On the other hand, the delay between the input signal and the echo signal must be small enough because otherwise part of the echo cancellation filter is inefficient. In the ideal case,you code would look like:
```C
write_to_soundcard(echo_frame, frame_size);
read_from_soundcard(input_frame, frame_size);
speex_echo_cancellation(echo_state, input_frame, echo_frame, output_frame);
```

If you wish to further reduce the echo present in the signal, you can do so by associating the echo canceller to the prepro-
cessor. This is done by calling:
```C
speex_preprocess_ctl(preprocess_state, SPEEX_PREPROCESS_SET_ECHO_STATE,echo_state);
```

every time an audio frame is played. Then, the capture context/thread calls:
```C
speex_echo_capture(echo_state, input_frame, output_frame);
```

for every frame captured. Internally, `speex_echo_playback()`simply buffers the playback frame so it can be used by `speex_echo_capture()` to call `speex_echo_cancel()`. A side effect of using this alternate API is that the playback audio is delayed by two frames, which is the normal delay caused by the soundcard. When capture and playback are already synchro-
nised, `speex_echo_cancellation()` is preferable since it gives better control on the exact input/echo timing.

The echo cancellation state can be destroyed with:
```C
speex_echo_state_destroy(echo_state);
```
It is also possible to reset the state of the echo canceller so it can be reused without the need to create another state with:
```C
speex_echo_state_reset(echo_state);
```

### Jitter Buffer
The jitter buffer can be enabled by including:
```C
#include <speex/speex_jitter.h>
```
and a new jitter buffer state can be initialised by:
```C
JitterBuffer *state = jitter_buffer_init(step);
```
where the `step` argument is the default time step (in timestamp units) used for adjusting the delay and doing concealment.A value of 1 is always correct, but higher values may be more convenient sometimes. For example, if you are only able to do concealment on 20ms frames, there is no point in the jitter buffer asking you to do it on one sample. Another example is that
for video, it makes no sense to adjust the delay by less than a full frame. The value provided can always be changed at a later
time.

The jitter buffer API is based on the `JitterBufferPacket` type, which is defined as:
```C
typedef struct {
char *data; /* Data bytes contained in the packet */
spx_uint32_t len; /* Length of the packet in bytes */
spx_uint32_t timestamp; /* Timestamp for the packet */
spx_uint32_t span; /* Time covered by the packet (timestamp units) */
} JitterBufferPacket;
```

As an example, for audio the timestamp field would be what is obtained from the RTP timestamp field and the span would be the number of samples that are encoded in the packet. For Speex narrowband, span would be 160 if only one frame is included in the packet.

When a packet arrives, it need to be inserter into the jitter buffer by:
```C
JitterBufferPacket packet;
/* Fill in each field in the packet struct */
jitter_buffer_put(state, &packet);
```

When the decoder is ready to decode a packet the packet to be decoded can be obtained by:
```C
int start_offset;
err = jitter_buffer_get(state, &packet, desired_span, &start_offset);
```

If `jitter_buffer_put()` and `jitter_buffer_get()` are called from different threads, then you need to protect
the jitter buffer state with a mutex.

Because the jitter buffer is designed not to use an explicit timer, it needs to be told about the time explicitly. This is done
by calling:

```C
jitter_buffer_tick(state);
```

This needs to be done periodically in the playing thread. This will be the last jitter buffer call before going to sleep (until
more data is played back). In some cases, it may be preferable to use
```C
jitter_buffer_remaining_span(state, remaining);
```
The second argument is used to specify that we are still holding data that has not been written to the playback device.
For instance, if 256 samples were needed by the soundcard (specified by `desired_span`), but `jitter_buffer_get()`
returned 320 samples, we would have `remaining=64`.

### Resampler
Speex includes a resampling modules. To make use of the resampler, it is necessary to include its header file:
```C
#include <speex/speex_resampler.h>
```

For each stream that is to be resampled, it is necessary to create a resampler state with:
```C
SpeexResamplerState *resampler;
resampler = speex_resampler_init(nb_channels, input_rate, output_rate, quality, &err);
```

where nb_channels is the number of channels that will be used (either interleaved or non-interleaved), input_rate is the sampling rate of the input stream, output_rate is the sampling rate of the output stream and quality is the requested quality
setting (0 to 10). The quality parameter is useful for controlling the quality/complexity/latency tradeoff. Using a higher
quality setting means less noise/aliasing, a higher complexity and a higher latency. Usually, a quality of 3 is acceptable for
most desktop uses and quality 10 is mostly recommended for pro audio work. Quality 0 usually has a decent sound (certainly
better than using linear interpolation resampling), but artifacts may be heard.
The actual resampling is performed using
```C
err = speex_resampler_process_int(resampler, channelID, in, &in_length, out, &out_length);
```

where channelID is the ID of the channel to be processed. For a mono stream, use 0. The in pointer points to the first sample
of the input buffer for the selected channel and out points to the first sample of the output. The size of the input and output
buffers are specified by in_length and out_length respectively. Upon completion, these values are replaced by the number of
samples read and written by the resampler. Unless an error occurs, either all input samples will be read or all output samples
will be written to (or both). For floating-point samples, the function speex_resampler_process_float() behaves similarly.
It is also possible to process multiple channels at once.

