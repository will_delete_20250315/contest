#### 一、库实现方式

- 编程语言：C++
  
- 使用方式：include头文件
  

#### 二.依赖分析

- 库本身功能无第三方库依赖
  
- 单元测试部分依赖googletest库，[google/googletest: GoogleTest - Google Testing and Mocking Framework (github.com)](https://github.com/google/googletest)
  

#### 三、license以及版权

Copyright (C) 2015 THL A29 Limited, a Tencent company, and Milo Yip. All rights reserved.

If you have downloaded a copy of the RapidJSON binary from Tencent, please note that the RapidJSON binary is licensed under the MIT License.

If you have downloaded a copy of the RapidJSON source code from Tencent, please note that RapidJSON source code is licensed under the MIT License, except for the third-party components listed below which are subject to different license terms. Your integration of RapidJSON into your own projects may require compliance with the MIT License, as well as the other licenses applicable to the third-party components included within RapidJSON. To avoid the problematic JSON license in your own projects, it's sufficient to exclude the bin/jsonchecker/ directory, as it's the only code under the JSON license.

#### 四、最新一次版本

2016年8月25日，版本号v1.1.0

#### 五、功能点分析

##### 功能

- 将JSON字符串解析为内部Document对象，然后就可以利用功能强大的Document和Value下的各种api轻松查询及修改DOM，并最终转换回JSON
  
- 可以自己创建并修改Value和Document对象，从而生成JSON字符串
  

##### 特点

- 跨平台
- 容易安装：只有头文件的库，只需把头文件复制至你的项目中。
- 独立、最小依赖
  - 不需依赖 STL、BOOST 等。
  - 只包含 `<cstdio>`, `<cstdlib>`, `<cstring>`, `<inttypes.h>`, `<new>`, `<stdint.h>`。
- 没使用 C++ 异常、RTTI
- 高性能
  - 使用模版及内联函数去降低函数调用开销。
  - 内部经优化的 Grisu2 及浮点数解析实现。
  - 可选的 SSE2/SSE4.2 支持。

#### 六、代码规模

| 类型  | 代码行数 |
| --- | --- |
| 库文件 | 12950 |
| 示例代码 | 1227 |
| 单元测试代码 | 9522 |
| 总计  | 23699 |