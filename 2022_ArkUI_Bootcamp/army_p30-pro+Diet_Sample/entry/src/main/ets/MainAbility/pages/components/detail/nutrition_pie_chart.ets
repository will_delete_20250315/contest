import { FoodInfo } from '../../../model/type/DietType'
import { NutritionElement } from '../../../model/DataModels'
import { CIRCLE_RADIUS } from '../../../utils/Constants'

import { CardTitle } from './card_title'

@Component
export struct NutritionPieChart {
    private foodInfo: FoodInfo
    private nutritionElements: NutritionElement[]
    private settings: RenderingContextSettings = new RenderingContextSettings(true)
    private context: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings)

    build() {
        Column() {
            CardTitle({ title: $r('app.string.nutrition_element'), subtitle: $r('app.string.unit_weight') })
            Canvas(this.context)
                .height(CIRCLE_RADIUS * 2)
                .aspectRatio(1)
                .margin({ top: 30, bottom: 32 })
                .onReady(() => {
                    this.nutritionElements.forEach((item) => {
                        this.context.beginPath()
                        this.context.moveTo(CIRCLE_RADIUS, CIRCLE_RADIUS)
                        this.context.arc(CIRCLE_RADIUS, CIRCLE_RADIUS, CIRCLE_RADIUS, item.beginAngle, item.endAngle)
                        this.context.fillStyle = item.color
                        this.context.fill()
                    })
                })
            Row() {
                ForEach(this.nutritionElements, (item: NutritionElement) => {
                    Row({ space: 4 }) {
                        Circle({ width: 8, height: 8 }).fill(item.color)
                        Text(item.element).fontSize(12)
                        Text($r('app.string.weight_with_gram_unit', item.weight.toString())).fontSize(12)
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceAround)
        }
        .cardStyle()
    }
}

@Styles function cardStyle () {
    .height('100%')
    .padding({ top: 20, right: 20, left: 20 })
    .backgroundColor(Color.White)
    .borderRadius(12)
}