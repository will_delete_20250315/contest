import { OneMealStatisticsInfo } from '../../../../model/DataModels'
import { MealFoodInfo } from '../../../../model/DataModels'
import { updateDietWeight } from '../../../../model/data/DataUtil'

import { CustomCounter } from './custom_counter'

@Component
export struct MealCard {
    @ObjectLink mealInfo: OneMealStatisticsInfo

    build() {
        Column() {
            if (this.mealInfo.mealFoods.length > 1) {
                // 标题（早餐、午餐、晚餐、宵夜）
                Text(this.mealInfo.mealTime.name)
                    .fontSize(24)
                    .fontFamily('HarmonyHeTi-Medium')
                    .height(56)
                    .width('100%')
                    .backgroundColor('#FFFFFF')
                    .padding({ left: 16, right: 16 })
                // 用餐食物（草莓、甜点、核桃.....）
                ForEach(this.mealInfo.mealFoods, (mealItem) => {
                    // 饮食详情
                    MealFoodDetail({ mealFoodInfo: mealItem })
                })
            }
        }
        .backgroundColor(Color.White)
        .borderRadius(12)
        .padding({ left: 16, right: 16 })
        .margin(12)
    }
}


@Component
struct MealFoodDetail {
    @Consume("dataChange") notifyDataChange: number
    @State shown: boolean = true
    @State translateX: number = 0
    private mealFoodInfo: MealFoodInfo

    aboutToAppear() {
        console.log('xx '+JSON.stringify(this.mealFoodInfo))
    }

    build() {
        if (this.shown) {
            if (this.mealFoodInfo.weight != 0) {
                Row() {
                    Row() {
                        Image(this.mealFoodInfo.image)
                            .width(50)
                            .height(50)
                        Column() {
                            Text(this.mealFoodInfo.name)
                                .fontSize(16)
                                .fontColor('#444444')
                                .fontFamily('HarmonyHeTi')
                                .margin({ bottom: 4 })
                            Text($r('app.string.calorie_with_kcal_unit', (this.mealFoodInfo.calories / 100).toString()))
                                .fontSize(11)
                                .fontColor('#A3A3A3')
                                .fontFamily('HarmonyHeTi')
                        }.alignItems(HorizontalAlign.Start)

                        Blank()
                        CustomCounter({
                            value: this.mealFoodInfo.weight + 'g',
                            onDec: () => {
                                if (this.mealFoodInfo.weight > 0) {
                                    this.mealFoodInfo.weight -= 50
                                    updateDietWeight(this.mealFoodInfo.recordId, this.mealFoodInfo.weight)
                                    this.notifyDataChange++
                                }
                            },
                            onInc: () => {
                                this.mealFoodInfo.weight += 50
                                updateDietWeight(this.mealFoodInfo.recordId, this.mealFoodInfo.weight)
                                this.notifyDataChange++
                            }
                        })
                    }
                    .width('100%')

                    Image($r("app.media.ic_public_delete"))
                        .backgroundColor('#E84026')
                        .objectFit(ImageFit.ScaleDown)
                        .borderRadius(20)
                        .margin({ left: 50 })
                        .size({ width: 40, height: 40 })
                        .onClick(() => {
                            animateTo({ duration: 400 }, () => {
                                this.shown = false
                            })
                        })
                }
                .transition({ type: TransitionType.Delete, translate: { x: -350, y: 0 }, opacity: 0 })
                .translate({ x: this.translateX })
                .width('100%')
                .height(70)
                .gesture(
                PanGesture()
                    .onActionUpdate((event: GestureEvent) => {
                        if (event.offsetX < 0) {
                            if (event.offsetX < -100) {
                                this.translateX = (event.offsetX * Math.exp(-1.848)) - 100
                            } else {
                                this.translateX = event.offsetX
                            }
                        }
                    })
                )
            }
        }
    }
}

