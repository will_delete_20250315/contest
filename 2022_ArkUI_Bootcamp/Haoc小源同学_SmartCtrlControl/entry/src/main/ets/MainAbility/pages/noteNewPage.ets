import router from '@ohos.router';
import { note, NoteArray } from '../model/noteModel'
import { getDate, getTime } from '../utils/functionUtil'

/**
 * 新建备忘录笔记编辑界面
 */
@Entry
@Component
struct noteNewPage {
  scroller: Scroller = new Scroller()
  @State date: number = router.getParams()['date']
  @State time: number = router.getParams()['time']
  title: string = ''
  @State noteValue: string = ''
  @State count: number = this.noteValue.length

  build() {
    Column({ space: 10 }) {
      Row() {
        Image($r('app.media.ic_public_back'))
          .width(24)
          .height(24)
          .onClick(() => {
            router.replace({
              url: 'pages/FamilyNote',
            })
          })
        Blank()
        if (this.noteValue == '') {
          Image($r('app.media.ic_public_ok_unabled'))
            .width(24)
            .height(24)
        } else {
          Image($r('app.media.ic_public_ok_filled'))
            .width(24)
            .height(24)
            .onClick(() => {
              this.title = this.title == '' ? this.noteValue.slice(0, 16) : this.title
              NoteArray.push(new note(this.title, getDate(), getTime(), this.noteValue))
              router.replace({
                url: 'pages/FamilyNote',
              })
            })
        }
      }
      .width('100%')
      .padding({ left: '2%' })
      .alignItems(VerticalAlign.Center)

      Scroll(this.scroller) {
        Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Start }) {
          TextArea({ placeholder: '标题' })
            .fontSize(30)
            .placeholderFont({ size: 30 })
            .fontWeight(FontWeight.Medium)
            .height(50)
            .backgroundColor($r('app.color.background_white'))
            .onChange((value: string) => {
              this.title = value
            })
          Text(this.date + ' ' + this.time)
            .fontColor($r('app.color.placeholderColor_grey'))
            .fontSize(14)
            .padding({ left: '2%' })
          TextArea({ text: this.noteValue })
            .fontSize(18)
            .backgroundColor($r('app.color.background_white'))
            .flexGrow(1)
            .onChange((value: string) => {
              this.noteValue = value
              this.count = value.length
            })
          Row() {
            Text('共' + this.count + '字')
              .fontColor($r('app.color.placeholderColor_grey'))
              .fontSize(14)
          }
          .margin({ top: 5 })
          .width('100%')
          .justifyContent(FlexAlign.End)
          .alignItems(VerticalAlign.Bottom)

          Row()
            .visibility(Visibility.Hidden)
            .height(40)
        }
        .height('100%')
      }
    }
    .padding({ top: 14, right: '3%', left: '8%' })
  }
}