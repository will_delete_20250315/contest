/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import reminderAgent from '@ohos.reminderAgent'
import { SubscribeData, getSubscribeData } from '../model/subscribeDataModels'

let calendar = null
let subscribeItems: SubscribeData[] = getSubscribeData()
let num = 0

@Component
export struct SwiperItem {
  private index: number
  private isSubscribe: boolean

  onAccept() {
    console.log("onAccept")
    if (calendar == null) {
      console.log("the calendar reminder is null")
      return
    }
    reminderAgent.publishReminder(calendar, (err, reminderId) => {
      console.info('num=' + num + ', reminderId:' + reminderId)
      subscribeItems[num].isSubscribe = true
      subscribeItems[num].reminderId = reminderId
    })
  }


  build() {
    Stack({ alignContent: Alignment.BottomStart }) {
      Image(subscribeItems[this.index].image)
        .objectFit(ImageFit.Auto)
      Text(subscribeItems[this.index].title)
        .fontSize(15)
        .fontColor(Color.White)
        .margin({ left: '5%', bottom: '10%' })
    }
  }
}

@Component
export struct SubscribeSwiper {
  private index: number = 0

  build() {
    Swiper() {
      ForEach(subscribeItems, item => {
        SwiperItem({ index: item.id })
      }, item => item.title)
    }
    .width('100%')
    .height('203')
    .indicator(false)
    .index(this.index)
    .autoPlay(true)
    .itemSpace(15)
    .displayMode(SwiperDisplayMode.AutoLinear)
  }
}