# ArkuiMenu

#### 介绍
HarmonyOS ArkUI入门训练营之健康生活实战 学习记录
这是一个记录饮食和查看食物信息的应用，主要用于管理饮食健康。可以添加饮食信息，包括食物的种类、重量以及用餐时间，如早餐、 午餐、晚餐和夜宵，并能统计得到相应用餐时间的总热量值、总蛋白质、总脂肪和总碳水值，并且用柱状图的形式展示出来

#### 学到知识
1.  启动页Canvas动画
2.  组件化
3.  图表制作

### 预览效果
| 中文  | 英文  |
|---|---|
| ![启动页](screenshots/device/logo_zh.png)  | ![启动页](screenshots/device/logo_en.png)  |
| ![网络布局](screenshots/device/FoodGrid_zh.png)  | ![网络布局](screenshots/device/FoodGrid_en.png)  |
| ![列表布局](screenshots/device/FoodList_zh.png)  | ![列表布局](screenshots/device/FoodList_en.png)  |
| ![食物详情](screenshots/device/FoodDetail_zh.png)  | ![食物详情](screenshots/device/FoodDetail_en.png)  |
| ![添加记录](screenshots/device/FoodDetail_Dialog_zh.png)  | ![添加记录](screenshots/device/FoodDetail_Dialog_en.png)  |
| ![记录页](screenshots/device/Record_zh.png)  | ![记录页](screenshots/device/Record_en.png)  |

