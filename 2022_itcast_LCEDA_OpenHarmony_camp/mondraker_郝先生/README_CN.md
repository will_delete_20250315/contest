## 此为第一次参加立创eda训练营，首先感谢立创的打样券与器件券的大力支持！！！感谢传智教育的老师答疑解惑！！！

### **省流：**

如果你想系统学习此次训练营的话，如下链接包含所有教学视频与软件资料，我就不过多讲解教学性的东西了：
[机器狗训练营作业提交表（立创&传智） \(qq.com\)](https://docs.qq.com/sheet/DWGxocU1aalRJS0Z2?u=ae0983b5d42e49d59f884ded1afb6dfa&tab=53shbm)；
如果你想学习机械结构，步态正逆解，机械装配等等，其他朋友的文档写的很清楚，我主要展示我的电路与软件；
如果基础比较薄弱可以看传智官方的b站视频；
如果还想进阶可以看bearpi的b站视频和鸿蒙官方文档；
结尾文档全的一批！！！

### **对于此次训练营**

* **全部外设使用HI3861模块独立驱动，并未使用其它单片机作弊或协助计算处理处理。**
* **对鸿蒙有了新的认知，对快速上手一个新的单片机、操作系统有了更深的理解，受益匪浅。**
* **系统学习了solid works21，学习了对于产品级别的建模。**
* **为方便大家学习验证，我直接将我的app目录以及所有测试固件开源放在附件里方便大家二次开发学习**

# 下面将分电路部分与软件部分讲解：
# **电路部分**


## **开发板介绍：**

    板载传智HI3861鸿蒙IOTWiFi开发模组，PCA9685-PWM控制芯片（最高支持16路舵机或ledPWM驱动），CH340n-usb转ttl烧录芯片。

    适合新手学习openharmony系统，机器人，机器狗等等。

## **一些“亮点”：**

*       引出模组io7引脚接led做为板载点灯（赋予生命）引脚，因为其同时还为模组PWM0引脚，所以还可以做呼吸灯等等；
*       电源指示灯与RST引脚接在一起可以清晰知道RST是否被按下,模组en是否拉高，调试时可以排除模组是否烧毁的情况；
*       下载器与主板隔离开，两者通过跳帽连接，可以给其他开发板烧录，节约社会资源与开发成本（省钱），或裁剪下来单独使用；
*       为防止按钮漏在板外侧误触，将RST按键放在边框内，且在按钮处将板边内凹（参照德仪开发板设计），按下时方便，精准，舒适；
*       模块全引脚引出（插针/插座），pca9685全引脚引出（标准舵机三线插槽），3v3/5v/vusb全电压引出，预留0.96oled屏iic接口。
*       丝印清晰，标注详细，设计精美。

## **改进建议：**

### 问题一：不知道电池电量？

解决办法：这个板子如果做小了这个电压检测电路也不太好加，除非是把7.4v锂电池充电管理电路设计在开发板上，有需求的朋友可以试试如下方案：

*      TP5100 (技新课堂讲过,不了解的可以看一下b站课程，讲的很不错！）
*      12v输入，充7.4v电池，还要买个12v/dc口充电器，不适合小白，不如直接买个电池充电器，Pass掉。
*      3.7v供电，芯片3.3v需要ldo,舵机5v需要dcdc,还需要充放电管理电路，对新手不太友好。

### 问题二：为什么不把其他传感器设计到板子上？

解决办法：不用繁琐的走线，避免复杂的焊接，买传感器模块能重复使用。这只是一块学习板，要发挥学习板的优势（小，方便）🐕。如果不是有其他板子需要验证，可能我会直接搞一个传感器扩展版出来。

## **PCB实物展示**

加热台、风枪焊接难度：⭐
刀头烙铁焊接难度：⭐⭐
![FCF374BCCF9574905A5BAA6B0F94891B.jpg](//image.lceda.cn/pullimage/YwmTqGTS7bfBZifs14u2M1NvVJFaraNn4v9NZW8o.jpeg)
![6852C71A34CDE74FE1A67AC602E943A5.jpg](//image.lceda.cn/pullimage/86DZZg0IyuJVVZgftHGyz6H3gzHfoVTwwQ5E5P1Z.jpeg)
还算比较精致吧。。。

## **焊接注意事项：**

* 3861模组先焊固定一个引脚再焊接其他脚，尽量不要用热台焊接模组类芯片（回流焊除外），锡膏放多模组一压容易在底下连锡。
* 3861模组建议不要拖焊接，容易连锡，新手不好处理（老手除外），如果连锡不要紧张，先把烙铁头擦干净上点新锡上去，再在连锡的地方靠一下把锡吸到烙铁上就行了。
* pca9685用拖焊的方法可以两下搞定，由于地址线引脚都接地了所以那里容易连锡，虽然问题不大但是不好看，解决办法就是先把那块gnd拿烙铁加热一会再焊接。
* 防止高温长时间炙烤焊盘和元器件，焊盘会掉，元件会坏。
* 焊接完成务必检查是否有短路情况，芯片价格都比较高，我真的会心疼🆗。

# \-\-\-软硬件绝缘线\-\-\-

## **讲了这么久，你的狗呢？**

全纸盒子制作，拿不出手（~~不是~~），不会省钱的设计师不是好设计狮🦁（~~嘴硬~~）。
实际上是在肝雪花灯外壳的3D文件，这次参加训练营主要目的之一除了学习鸿蒙就是学习solidworks，都说机械电子不分家，这次我是彻底体会到了！！！
从零开始学画一把锤子到现在已经可以自主建模装配与渲染了，外壳文件已经移交给亚克力厂切割了，拿到手合适就会立刻开源了（超级好看！！！）

## 下面请欣赏纸狗🐕：

![0662F01BAF05BD10027657537294DE13.jpg](//image.lceda.cn/pullimage/zcJLo8BkNArg5oCiuf92ahgcdg5tJrAqBskJHyFr.jpeg)
![B3B4392F2264E6684FE04F1EFD79AC55.jpg](//image.lceda.cn/pullimage/PVHCI8sXjnR8lCglYAtdFwv9AM2XjjHNh5zIXetN.jpeg)

#### 这么一看好像还挺精致的😁😁😁

#### 当然了，想要0元做狗子是要付出很多手工时间的，而且手工测量与制作误差大，纸板子也不是很结实，为此我还专门做了无用功，初代狗子长这样：![9524C894274915584EFDD01FAEBA8E51.jpg](//image.lceda.cn/pullimage/RCN7d7JvQ54qiff5mNvr9zS2nONTmAfPkOkHshSu.jpeg)

![7B0D510537AF20E2CD10E74603674A8F.jpg](//image.lceda.cn/pullimage/wbDysfa1EsNNPg1XzWAcX9HVwqkdeeiF3hCPvpgB.jpeg)

#### 当时还觉得自己做的不错，结果最后发现在绝对的重量面前，就是个P\*\*\*。。。
正因如此我的狗子才在评奖前一天堪堪完成了，还好在前几天在软件上下了功夫，至少调起来不是太麻烦啊🤗🤗🤗

# \-\-\-软硬件绝缘线\-\-\-
# **软件部分**

## **写在前面**

* 如果你有esp8266或者stm32等等开发经验，那我的建议是直接打开鸿蒙的目录页（各个文件功能参考鸿蒙官方文档），挨个文件、源代码打开浏览，看其底层与8266或者stm32有什么不一样的地方，比如基本的io功能设置，i2c读写以及和8266对应的设置AP等等。熟练之后就可以直接移植库了（比如我的mpu6050模块库就移植与stm32，imu对应的数学计算库直接调用了肖总提供的示例代码**kinematics.h**,SSD1306、ST7735等等也是也是移植的库）。不过我还是非常建议你看一下下面的课，看完就能解除在看文档时候遗留下来的很多困惑。
* 我采用linux环境下CLion编译固件。

## **鸿蒙入门（传智老师的配套课程，非常非常非常保姆级教程）：**

* 如果你喜欢使用C语言开发，那么看这个就算入门了；

1. B站链接🔗：[https://www\.bilibili\.com/video/BV1tP4y157ZL?share\_source=copy\_web&vd\_source=2817d7288773e9a09a9119dbad06ee11](https://www.bilibili.com/video/BV1tP4y157ZL?share_source=copy_web&vd_source=2817d7288773e9a09a9119dbad06ee11)
2. 文档资料链接：[定时器 - 传智鸿蒙元气派 \(czxy.com\)](https://robot.czxy.com/ohos/day05/timer.html)

## **硬件检查**

    相应的，板子焊接好了该如何进行检查呢？

## 点灯呗

    首先查文档得：

![Snipaste_2022-07-20_04-32-53.jpg](//image.lceda.cn/pullimage/ub0S6OEhPvKRdSrvYM0oUaQcNq1uWOB8nJujGM14.jpeg)

    底层硬件软件驱动都在base里，我们浏览base发现：

![Snipaste_2022-07-20_04-36-39.jpg](//image.lceda.cn/pullimage/zRP7wo4yTM6jnqFAWnvLj9S7TjAcKRS9MX4Ro6SP.jpeg)

    立刻找到了我们想要的东西，底层驱动封装库。篇幅有限我也不再赘述了，大家感兴趣可以自己打开研究，英语不好的可以用qq全屏翻译。
之后直接按照stm32的点灯流程写一遍就行了，别忘了在app目录与项目文件夹下的BUILD.gn文件内编辑相应信息。

## **测试代码**

    为了检测i2c通信与pca9685芯片的焊接好坏，我专门编写了一个测试固件，可以循环检测16通道的pwm是否正常并且在ssd1306 oled上显示出来，训练营的群里我麻烦莫工上传了，这里我也会放在附件里。效果大概是下面这样。
![BD87EDD9AF03C137B0851F2EAA5CF148.jpg](//image.lceda.cn/pullimage/pM0SZNSALIRkgpxTITU0CZqiMAawpINbybsQ20aJ.jpeg)
还有用PWM0脚（gpio7）演奏两只老虎(见附件)

## **介绍我的狗🐕**

* 由于是第一次参加训练营，所以很多东西比较紧张，很多东西也是第一次做，不是非常完美，如果参加了第一期鸿蒙智能开关的话应该现在就能做出完美的成品了。时间紧迫mpu6050控制舵机的算法也还没有实现，本来还想用DevEco Studio写一个鸿蒙app（学习了但没时间做出来）等等等等。在介绍之后肯定会有自己的改进意见，如果大家还有什么意见可以提在评论区里共同学习进步。
* mpu6050模块（模块地址：`0x68`），0.91寸oled-ssd1306模块（模块地址：`0x78`），pca9685模块（模块地址：`0x40`）全部作为SLAVE挂载在HI3861模块硬件I2C0上，引脚使用IO10\(I2C0\_SDA\)与IO9\(I2C0\_SCL\)\.。0\.91寸显示屏主要用来显示mpu6050解算出的欧拉角。

*`IoTI2cInit(I2C_IDX, 400*1000); /* baudrate: 400kbps */*`*
*`    IoTIoSetFunc(IOT_IO_NAME_10, IOT_IO_FUNC_10_I2C0_SDA);`*
*`    IoTIoSetFunc(IOT_IO_NAME_9, IOT_IO_FUNC_9_I2C0_SCL);`*

* *1.44寸ST7735彩屏与模块之间采用SPI通讯，由于仅一个SPI设备，所以CS片选直接拉低节省本来就不多的模块引脚，如果你需要的话，可以自己添加上CS片选与ledk的pwm调光。ST7735主要用来显示小狗表情等等。*

*`//时钟和数据线`*
*`  IoTIoSetFunc(IOT_IO_NAME_6, IOT_IO_FUNC_6_SPI0_CK);`*
*`    IoTIoSetFunc(IOT_IO_NAME_8, IOT_IO_FUNC_8_SPI0_TXD);`*
*`    IoTIoSetDriverStrength(IOT_IO_NAME_6,IOT_IO_DRIVER_STRENGTH_2);`*
*`    //RES`*
*`    IoTIoSetFunc(IOT_IO_NAME_5, IOT_IO_FUNC_5_GPIO);`*
*`    IoTGpioSetDir(IOT_IO_NAME_5, IOT_GPIO_DIR_OUT);`*
*`    //DC`*
*`    IoTIoSetFunc(IOT_IO_NAME_7, IOT_IO_FUNC_7_GPIO);`*
*`    IoTGpioSetDir(IOT_IO_NAME_7, IOT_GPIO_DIR_OUT);`*

* *超声波传感器，温湿度传感器等等的库移植就比较简单了，此就不赘述。*
* *控制小狗部分采用原先的HI3861做AP进行内网登陆控制，用html/CCS重新优化了网页，学习了一段时间华为应用开发软件DevEco Studio，本来是想要给狗独立设计一个hpp,用我的华为手机碰一碰实现联网再进行hpp操控，结果狗腿拉跨耽误了三四天的时间（泪目）。。。。。。*

## **已实现：**

## ***1.mpu6050模块***

*参考了小熊pi利用6050模块制作的智慧井盖。还有其他文档也可供学习移植。*

*[BearPi-HM_Nano: 小熊派BearPi-HM Nano开发板基于HarmonyOS的源码 - Gitee.com](https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/sample/C4_e53_sc2_axis)*

*mpu6050模块与HI3861模组通过模块IO10，IO9进行I2C通讯，在串口助手打印四元数解算出的欧拉角，加速度，角速度，温度数据。*

*移植**imu库**进行<span class="colour" style="color:rgb(51, 51, 51)">四元数姿态解算</span>，系统学习并且利用源码中**libm_port数学库**辅助计算。*

*`Pitch  = asin(2`*`(q0*q2 - q1*q3 )) * 57.2957795f;      `
`//Roll   = asin(2*(q0*q1 + q2*q3 )) * 57.2957795f;`
`Roll = atan2(2*(q2*q3 + q0*q1), -2*q1*q1-2*q2*q2+1) * 57.2957795f; `
`Yaw  = atan2(2*(q1*q2 + q0*q3),q0*q0+q1*q1-q2*q2-q3*q3) * 57.2957795f;`

定义结构体类型方便后期输出对应数据：

![image.png](//image.lceda.cn/pullimage/VOOOq54XaF1rnpgwqe6VbBeqpWsz6mbszYoTN82O.png)

实物运行效果：（例程放在附件中）

![B748DDE572FEEEADA9A5511080F7CD8E.gif](//image.lceda.cn/pullimage/d2cDsI8gezWaERxGTxe9O2WpCJSBh932zyb3nqDR.gif)

## ***2.ST7735模块***

移植自ST7789模块，发现颜色反转，大小不合适，屏幕也镜像翻转显示。
查看ST7735数据手册找到控制RGB以及屏幕反转显示的寄存器：

![image.png](//image.lceda.cn/pullimage/NuLXnnr8FC79vGmBwG1MLt9Xk5qZ9qavdVWNBZ83.png)

先注释掉初始化里反转颜色的写21h寄存器命令：

`//LCD_WR_REG(0x21);//控制反转颜色`

再直接写36h寄存器控制颜色反转和屏幕旋转和屏幕镜像：

`void LCD_Rotation(void){`
`    LCD_WR_REG(0x36);`
`    LCD_WR_DATA8(0X00);//0X08`
`}`

之后直接在宏定义更改屏幕尺寸：

`#define LCD_W 128`
`#define LCD_H 128`

要显示128*128图片需要计算BGR数组大小：*

*![image.png](//image.lceda.cn/pullimage/XEsoHxBx6ubekjNIpLXRqv2p2m3Vs5ALy6ApzspD.png)*

*换算成16进制为：`const uint16_t gif0[0x4000] `*

*显示语句为：`LCD_ShowGif(0,0,128,128,gif[i%3],0x4000);`*

### ***转换16进制数组：***

*众所周知**Image2lcd**只能导出类似于：`0x00`格式数组，但是使用的为:`0x0000`格式，所以使用我附件提供的转换软件即可：*
*![image.png](//image.lceda.cn/pullimage/jyeIOkoAm3QZwYImMryVGnvcPH8nLspoMShkWr4V.png)*

*如图设置，导入图片必须为bmp格式。导出.h打开之后长这样，复制这个数组就可以了：*

*![image.png](//image.lceda.cn/pullimage/TTSPjg5rDnev57yKBlDXIaRsKObnvNp0C6jkE9Xs.png)*
*设置大小的话直接在画图里设置就行了：*
*![image.png](//image.lceda.cn/pullimage/K9CZkOUGDXLjfVhF5FGMfS0VmROHRgwWKgxEyIWi.png)*

*正常显示GIF图片效果：（测试代码放在附件）*
*我只扣了一张展示，我提供的软件可以批量转换生成多帧GIF动画，在最后有演示。*
*![02F5656095C04BEB9C3C60963596D5A7.jpg](//image.lceda.cn/pullimage/yF5DaUdFh89C6NihYfTSbZ9OD0tdbQFWfk1PEdgw.jpeg)*
*三四块钱的屏幕，显示效果有点拉跨。*

## ***3.ssd1306模块***

*不多介绍了，直接调用device目录下现有的库：*

*![image.png](//image.lceda.cn/pullimage/vyZvzBVK0x6Fol26woGVL0fIYA321pNINFf6sEfm.png)*

*修改BUILD.gn，添加库路径：*

*`"//device/itcast/genkipi/hi3861_adapter/hals/interfaces/car/include", #ssd1306`*

*打开函数库阅读就行了，由于我的是0.96寸，需要改一下寄存器初始化那里：*
*加上如下语句可以直接在宏定义里自由切换0.91/0.96*

*`    if (MAX_ROW == 32){`*
*`        SSD1306_write_cmd(0x02);`*
*`    }//0.96寸分辨率是128`*`64 ,设置12；0.91寸分辨率是128*32 ,设置02**************************************`
`    else if(MAX_ROW == 64){`
`        SSD1306_write_cmd(0x12);`
`    }`
`    if (MAX_ROW == 32){`
`        SSD1306_write_cmd(0x1f);`
`    }//--1/32 duty 默认0X3f(1/64) 0x1f(1/32)		0.96:0X3f ；0.91寸:0x1f**************************`
`    else if(MAX_ROW == 64){`
`        SSD1306_write_cmd(0x3f);`
`    }`

再就没啥了，自由移植字体，画图等等函数就可以了。

## ***4.PCA9685模块***

源代码里有写好的库，直接移植就可以了。
不懂的直接器件手册

## ***5.网页控制页面优化***

如果你和我一样希望优化原来的dog控制页面，那么改这里就可以了：
![image.png](//image.lceda.cn/pullimage/LsSbXVY2RB0wbAbDQwbHz19ZZpCk60NuK0t5rMPI.png)
如果你会html/ccs/java的话，这里非常好改，并且你还可以添加很多新功能与函数，但是这里因为要嵌入到c代码里非常麻烦，于是我专门写了一个python脚本转换格式，把写好的网页代码拼接成一行来显示（还可以在每行前后加入符号），有兴趣的小伙伴可以继续优化一下。（见附件）
![image.png](//image.lceda.cn/pullimage/vMDYhxgdDM6HcHjWp8Mrotvv0L2p389BJUZwBshX.png)
改好后的页面如下，增加了按钮的互动效果等等：
![9F00C62FF4BC145AF9255F559B7204F8.gif](//image.lceda.cn/pullimage/0cDiiu9K4WsFriYz7IaOveYKdKzXnjji2Kq6e0XA.gif)
**(肝不动了)**
**因为支持多线程的缘故，可以lcd与oled各干各的，大家也可以开发出更多好玩的项目来。比如lcd眨眼，oled显示数据（代码见附件）**

## ***6.多线程任务处理（内核开发初试）***

用一张图先表示一下
![Snipaste_2022-07-25_02-45-12.png](//image.lceda.cn/pullimage/FZaadwfCMPN218mMW5SCfIEh6LnpOABm0vV5fFlt.png)
多线程我也是第一次使用，以前一直使用单片机定时器回调之类的模拟多线程。这次阅读文档发现支持多线程，实际应用发现对与三个以上线程就比较吃力。这次测试开启了两个线程一个用于处理网页收发与lcd屏幕的显示，一个用于处理mpu6050与oled显示数据。
![image.png](//image.lceda.cn/pullimage/TKrBsqKrQzk2mM6U06GjERXB0SJMl5rkUnGgm29A.png)
实验结果是非常的nice！

## ***8.鸿蒙Hpp初试***

之前是想用HUAWEI的DevEco Studio给我的手表开发一个Hpp用来联网控制小狗的，奈何自己对前端js方面开发才疏学浅，实在是不太好操作，自己对照官方文档摸索了两天，踩了不少坑后，终于是写了一个模板出来，最后与手表用ip connection方法调试成功。（资料见附件）
![image.png](//image.lceda.cn/pullimage/lq0LFrW0RAwe6IF7eRTFyFYyEtrSHldy6QowoIyH.png)
完成后长这样
![1CE03F5D8E884822F86235690D7CD364.jpg](//image.lceda.cn/pullimage/PYFOSXItjYyFUvVZftxYcH2uHPNkArBlDIumR22j.jpeg)
![123143C1873C50AF74ABB881C178DD8D.jpg](//image.lceda.cn/pullimage/bwjVYtzBvBg3VxSTTR8Ylm1hbwQZshYQxsYT8slC.jpeg)
所以多掌握几门语言实在是太重要了，大四打算系统学一下js之类的，弥补一下自己在web与后端接口方面的弱项。

## **待完成以及建议**

#### 1.mpu6050姿态控制修狗舵机，达到在四脚高度不一致的情况下狗身与地面平行。
2.开发hpp应用程序或微信小程序为机器狗狗提供方便快捷稳定的控制环境。
3.加入离线语言控制模块（03T等等），移植简单且可玩性高，在离线语音芯片平台编程指令后，用rx/tx串口通信即可。
4.可以加入更多外设，摄像头等等，模组功能强大，可以直接处理这些数据。
5.我在尝试用他跑LVGL，不过头一次移植还在学习查找，大家感兴趣也可以试试。
6.网页控制界面还可以优化，只要搞清楚网页和设备之间的传输原理就可以了，后期准备加上网页向LCD传输图片等等有意思的功能。

# **最终展示（图片）**

### b站视频链接：【基于Hi3861的鸿蒙纸板狗[立创eda&传智训练营]-哔哩哔哩】 ![](file:///C:\Users\69180\AppData\Roaming\Tencent\QQTempSys\[5UQ[BL(6~BS2JV6W}N6[%S.png)[https://b23.tv/tdu9dGL](https://b23.tv/tdu9dGL)

### 模块接线图：

![image.png](//image.lceda.cn/pullimage/5N1qWJzkZ8Nr36NrfWhnYN65wSrs9Uz4a4OU6xTW.png)

### 实物图：

![DC68685A157AFE82C5F17B5D8E03F06B.jpg](//image.lceda.cn/pullimage/0junv7wyJU4RLRKoOdiwpJMe7ntwLaLgCMXwUVkl.jpeg)

###### ![CD60F95D0F4DC58A8A333BBB96C008A3.jpg](//image.lceda.cn/pullimage/8qybX6McYqS73JG6WE8eq2JGjpgqkv2Jo9oC8U8b.jpeg)

![69F31D0E014CA2F4D9DC705E9C86D622.jpg](//image.lceda.cn/pullimage/gIYK5JFoqiK0LQcHowM4TpyukkSXMp8ZbGgOqgNy.jpeg)

# **结语**

## **参与训练营确实学到很多东西，因为要考研比较忙，而且还有很多事情压在身上所以这次没有用亚克力器件完成。程序等等也没有写的很完善，只是大体搭建了框架出来，表示自己完全理解了并且创新了。以后会继续完善并且开发其他功能的！！**