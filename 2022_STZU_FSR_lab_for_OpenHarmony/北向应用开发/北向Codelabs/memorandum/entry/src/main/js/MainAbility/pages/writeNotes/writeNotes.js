import router from '@ohos.router';
import systemTime from '@ohos.systemTime';

export default {
    data: {
        text: '',
        inputText: '',
        title: '',
        id: -1,
        textListener: true,
        titleListener: true,
        opacity: 0.3,
        code: -1
    },

    onInit() {
        let params = router.getParams()
        let info = params.info
        this.inputText = info.content
        this.code = info.code
        if (info.code == 0)this.id = info.id
        console.info('xxx--- receive ' + ' ' + info.code + ' ' + info.title + ' ' + info.content)
        if (info.title !== '')this.title = info.title
        systemTime.getDate((error, data) => {
            if (error) {
                console.error(`xxx--- failed to systemTime.getDate because ` + JSON.stringify(error));
                return;
            }
            console.log(`xxx--- systemTime.getDate success data : ` + JSON.stringify(data));
        });
        this.$watch('inputText', 'onChangeText')
        //        this.$watch('title', 'onChangeTitle')
    },

    onChangeText() {
        if (this.textListener === true) {
            this.textListener = false
            this.opacity = 1
        }
        if (this.inputText === '') {
            this.textListener = true
            this.opacity = 0.3
        }
    },
    onChangeTitle(e) {
        this.title = e.value
        console.info('xxx--- title change = ' + e.value)
        if (this.titleListener === true)
        this.titleListener = false
        if (this.title === '' || e.value === '') {
            this.titleListener = true
        }
    },


    onChange(e) {
        this.inputText = e.value
    },
    keyboard() { //---自定义键盘的事件，可以不用
        this.text = this.inputText
        this.$element('keyboard').show()
    },
    textClick(e) {
        this.text = e.detail.text
        this.inputText = this.text
    },

    back() {
        if (this.titleListener && this.textListener) {
            router.back()
        }
        else this.$element('alert').show()
    },

    update() {
        console.info(`xxx--- info submit ${this.code} ${this.title} ${this.inputText} `)
        router.clear()
        let info
        if (this.code == 0) {
            info = {
                id: this.id,
                title: this.title,
                content: this.inputText,
                code: this.code
            }
        } else {
            info = {
                title: this.title,
                content: this.inputText,
                code: this.code
            }
        }
        router.push({
            url: 'pages/index/index',
            params: {
                info: info
            }
        })
    },

    save() {
        if (this.titleListener === true) {
            this.setTitle(() => {
                this.update()
            })
        }
        else this.update()
    },

    setTitle(cb) {
        let length = this.inputText.length
        if (length > 5)
        this.title = this.inputText.slice(0, 5)
        else this.title = this.inputText.slice(0, length)
        cb()
    },
    backChoice(e) {
        let flag = e.target.id
        if (flag == 1)this.save()
        else {
            router.back()
        }
    }
}
