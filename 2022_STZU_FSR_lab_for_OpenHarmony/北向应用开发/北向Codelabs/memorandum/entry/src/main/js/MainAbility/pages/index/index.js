import router from '@ohos.router';
import rdbStore from '../../common/model/rdbServer'

const SQL_CREATE_TABLE = ["CREATE TABLE IF NOT EXISTS NOTES (ID INTEGER PRIMARY KEY AUTOINCREMENT, TITLE TEXT NOT NULL, CONTENT TEXT NOT NULL)"]

export default {
    data: {
        rdbStore: new rdbStore(SQL_CREATE_TABLE),
        list: [
            {
                id:3,
                title:'123',
                content:'24134'
            },
            {
                id:2,
                title:'123',
                content:'24134'
            },
            {
                id:1,
                title:'123',
                content:'24134'
            }
        ],
        deleteList: [],
        length: 0,
        deleteListener: false,
        isCheck: '',
        flag: true,
        search: true,
        animation: '',
        searchListener: false
    },
    onInit() {
    },
    insert(obj, cb) {
        this.rdbStore.insertValue('NOTES', obj)
        cb()
    },
    onShow() {
        let params = router.getParams()
        if (params) {
            let info = params.info
            let code = info.code
            let id = info.id
            let title = info.title
            let content = info.content
            console.info(`xxx--- info receive ${code} ${title} ${content}`)
            let obj = {
                title: title,
                content: content
            }
            if (code == 0) {
                this.update(obj, id, () => {
                    setTimeout(() => {
                        this.query()
                    }, 500)
                })
            }
            else {
                this.insert(obj, () => {
                    setTimeout(() => {
                        this.query()
                    }, 500)
                })
            }
        }
        else this.query()
    },

    update(obj, id, cb) {
        this.rdbStore.updateValue(obj, 'NOTES', 'ID', id)
        cb()
    },
    query() {
        let self = this
        this.rdbStore.queryValue('NOTES', (resultSet) => {
            self.resultSetServer(resultSet)
        })
    },

    click(title, content, id) {
        if (this.deleteListener === false) {
            router.push({
                url: 'pages/writeNotes/writeNotes',
                params: {
                    info: {
                        id: id,
                        title: title,
                        content: content,
                        code: 0
                    }
                }
            })
        }
    },

    add() {
        router.push({
            url: 'pages/writeNotes/writeNotes',
            params: {
                info: {
                    title: '',
                    content: '',
                    code: 1
                }
            }
        })
    },
    checkboxOnChange(id, e) { //---选择删除
        if (e.checked) {
            this.deleteList.push(id)
        } else {
            let index = this.deleteList.find(e => e === id)
            this.deleteList.splice(index, 1)
        }
    },
    deletePress(idx, id) {
        this.isCheck = idx
        this.deleteList.push(id)
        this.deleteListener = true
    },

    deleteNotes() {
        this.deleteListener = false
        if (this.deleteList.length > 0) {
            for (let i in this.deleteList) {
                this.rdbStore.deleteValue('NOTES', 'ID', this.deleteList[i])
            }
            this.deleteList.splice(0)
            setTimeout(() => {
                this.query()
            }, 300)
        }
    },
    cancelDelete() {
        this.deleteListener = false
    },

    search() {
        this.searchListener = true
    },
    cancelSearch() {
        this.searchListener = false
    },
    resultSetServer(resultSet) {
        let contactList = []
        var self = this
        if (resultSet.rowCount > 0) {
            while (resultSet.goToNextRow()) {
                let id = resultSet.getLong(resultSet.getColumnIndex("ID"));
                let title = resultSet.getString(resultSet.getColumnIndex("TITLE"));
                let content = resultSet.getString(resultSet.getColumnIndex("CONTENT"));
                let obj = {
                    id: id,
                    title: title,
                    content: content
                };
                contactList.push(obj);
            }
            if (contactList.length > 0) {
                this.flag = true
                self.list = contactList
                this.length = contactList.length
                console.info('xxx--- query suc length = ' + contactList.length)
            }
        } else {
            console.info('xxx--- query empty')
            this.length = 0
            this.flag = false
        }
        resultSet.close();
        resultSet = null;
    },
    onChange(e) {
        let val = e.value
        var self = this
        console.info('xxx--- search value change ' + val)
        this.rdbStore.search('NOTES', 'TITLE', val, (resultSet) => {
            self.resultSetServer(resultSet)
        })
    }
}



