var showContent=[]
export default {
    data: {
        username: "",
        password:'',
        text:'',
    },
    onInit() {
        let info = {
            id:'username',
            show:false,
            value:''
        }
        showContent.push(info)
        let info2 = {
            id:'password',
            show:false,
            value:''
        }
        showContent.push(info2)
    },
    textClick(e){
        this.text = e.detail.text
        for(let i=0;i<showContent.length;i++){
            if(showContent[i].show===true){
                showContent[i].value = this.text
            }
        }
        this.username=showContent[0].value
        this.password=showContent[1].value
    },
    usernameInput(){
        this.text = this.username
        showContent[0].show=true
        showContent[1].show=false
        this.$element('keyboard').show()
    },
    passwordInput(){
        this.text = this.password
        showContent[1].show=true
        showContent[0].show=false
        this.$element('keyboard').show()
    },

}
