# 南向设备开发技术博客

* [【FFH】BearPi_Micro业务和驱动代码_GPIO口使用](https://ost.51cto.com/posts/13314)

* [【FFH】BearPi-HM Micro 南向研究——GPIO驱动分析](https://ost.51cto.com/posts/13560)

* [【FFH】DevEco Device Tool一站式集成开发环境搭建](https://ost.51cto.com/posts/14753)

* [【FFH】DevEco Device Tool设备开发全流程概述](https://ost.51cto.com/posts/14769)

* [【FFH】DevEcoDeviceTool实战源码获取到HelloWorld](https://ost.51cto.com/posts/14773)
  * [源码Codelabs-my_first_app](../南向Codelabs)

* [【FFH】Hi3516DV300 OpenHarmony3.1环境配置及烧录](https://ost.51cto.com/posts/15162)

* [【FFH】DevEco Device Tool：HDF框架一键生成！ ](https://ost.51cto.com/posts/15388)
  * [源码Codelabs-hello_linux](../南向Codelabs)
* [ 【FFH】HDF驱动开发流程解析 ](https://ost.51cto.com/posts/15779)
  *  [源码Codelabs-hello_liteos](../南向Codelabs)

* [【FFH】BearPi_Micro南向开发I2C驱动代码 ](https://ost.51cto.com/posts/13317)

* [【FFH】BearPi_Micro南向开发I2C驱动代码深耕 ](https://ost.51cto.com/posts/13983)

* [【FFH】BearPi_Micro南向开发PWM驱动代码 ](https://ost.51cto.com/posts/13838)

* [【FFH】HDF驱动开发之编写驱动代码 ](https://ost.51cto.com/posts/13468)

* [【FFH】OpenHarmony设备开发基础知识(一）](https://ost.51cto.com/posts/15086)

* [【FFH】OpenHarmony设备开发基础（二）产品信息配置](https://ost.51cto.com/posts/15148)

* [【FFH】OpenHarmony设备开发基础（三）编译依赖](https://ost.51cto.com/posts/15134)

* [【FFH】OpenHarmony设备开发基础（四）启动流程](https://ost.51cto.com/posts/15259)

* [【FFH】OpenHarmony设备开发基础（五）GPIO点灯](https://ost.51cto.com/posts/15392)

* [【FFH】Hi3516DV300驱动开发——编写LED灯控制程序](https://ost.51cto.com/posts/15888)
  *  [源码Codelabs-LED_Control](../南向Codelabs)
  *  [ 演示视频 ](https://ost.51cto.com/show/15893)
* [【FFH】啃论文俱乐部——啃论文方法初探](https://ost.51cto.com/posts/16288/)
* [【FFH】Linux下配置小熊派-鸿蒙·叔设备开发（南向）的开发环境](https://ost.51cto.com/posts/10283)
* [【FFH】小熊派驱动开发流程（以点亮LED灯为例）](https://ost.51cto.com/posts/10304)
* [【FFH】小熊派纯代码开发流程](https://ost.51cto.com/posts/10330)
* [【FFH】小熊派驱动调用流程（以调用LED灯驱动为例）](https://ost.51cto.com/posts/10338)
* [【FFH】小熊派添加自定义JS API接口流程（以点亮LED为例）](https://ost.51cto.com/posts/10369)
* [【FFH】安装Ubuntu到移动介质并设置好UEFI引导 ](https://ost.51cto.com/posts/10448)
* [【FFH】来自OpenHarmony与苹果的梦幻联动！](https://ost.51cto.com/posts/10992)
* [【FFH】OpenHarmony与苹果的梦幻联动——服务器端（上）](https://ost.51cto.com/posts/12048)
* [【FFH】OpenHarmony与苹果的梦幻联动——服务器端（下）](https://ost.51cto.com/posts/12178)
* [【FFH】OpenHarmony 设备开发(一)-WIFI连接](https://ost.51cto.com/posts/16671)
    *  [源码Codelabs-WIFI_demo](../南向Codelabs/WIFI_demo)
* [【FFH】OpenHarmony设备开发（二）-基于TCP遥控小车](https://ost.51cto.com/posts/16704)
    *  [源码Codelabs-WIFI_car](../南向Codelabs/WIFI_car)
    *  [演示视频-双人遥控小车](https://ost.51cto.com/show/16728)