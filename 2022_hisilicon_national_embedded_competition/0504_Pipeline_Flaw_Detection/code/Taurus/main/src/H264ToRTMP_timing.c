#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mman.h>


#define __STDC_CONSTANT_MACROS

#ifdef __cplusplus
extern "C"
{
#endif
#include <libavformat/avformat.h>
#include <libavutil/mathematics.h>
#include <libavutil/imgutils.h>
#include <libavutil/time.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avio.h>
#include <libavutil/file.h>
#include <libavdevice/avdevice.h>

#include "sample_store_rtmp.h"

#ifdef __cplusplus
};
#endif

#define AV_IO_BUF_SIZE	(150*1024) //(96*1024)


unsigned int RTMP_frame_id = 0;
int RTMPThreadStart;
static pthread_t gs_VencRTMP;



int avError(int errNum) {
    char buf[1024];
    av_strerror(errNum, buf, sizeof(buf));
    printf("failed!\n");
    return -1;
}


int read_buffer(void *opaque, uint8_t *pbuf, int buf_size)

