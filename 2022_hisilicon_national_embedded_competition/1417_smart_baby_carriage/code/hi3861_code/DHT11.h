#ifndef __DHT11__H
#define __DHT11__H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "oled_ssd1306.h"
#include "iot_gpio_ex.h"
#define DHT11_GPIO 10

// uint8_t GPIOGETINPUT(unsigned int id,IotGpioValue *val)
// {
//     IoTGpioGetInputVal(id,val);
//     return *val;
// }
static void DHT11_IO_OUT(void)
{
    IoTGpioInit(DHT11_GPIO);
    //将GPIO10设置普通IO
    IoTGpioSetDir(DHT11_GPIO,IOT_GPIO_DIR_OUT);//将GPIO端口设置为输出模式
}
static void DHT11_IO_IN(void)
{
    IoTGpioInit(DHT11_GPIO);
     //将GPIO10设置普通IO
    IoTGpioSetDir(DHT11_GPIO,IOT_GPIO_DIR_IN);//将GPIO端口设置为输入模式
}
static void DHT11_RST(void)
{
    IoTGpioInit(DHT11_GPIO);
    //将GPIO10设置普通IO
    //hi_io_set_func(DHT11_GPIO,hi_io_func_gpio_10);
    IoTGpioSetDir(DHT11_GPIO,IOT_GPIO_DIR_OUT);//将GPIO端口设置为输出模式
    IoTGpioSetOutputVal(DHT11_GPIO,IOT_GPIO_VALUE0);//将总线拉低
    hi_udelay(20000);//延时20ms
    //GpioGetOutputVal(DHT11_GPIO,&value);
    IoTGpioSetOutputVal(DHT11_GPIO,IOT_GPIO_VALUE1);//拉高总线
    hi_udelay(30);//总线拉高20-40us
}
static int DHT11_Check(void)//DHT11状态检测当工作状态正常的情况下将返回0，工作异常将返回1
{
    uint8_t str[100];
    uint8_t ret;
    uint8_t retry=0;
    DHT11_IO_IN();
    IotGpioValue value = IOT_GPIO_VALUE1;
    //获取GPIO10引脚状态
    IoTGpioGetInputVal(DHT11_GPIO,&value);
    while(value==IOT_GPIO_VALUE1 && retry<100)
    {
        IoTGpioGetInputVal(DHT11_GPIO,&value);
        retry++;
        hi_udelay(1);
    }
    printf("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV222\r\n");
    if(retry>=100)return 1;
    else retry=0;
    IoTGpioGetInputVal(DHT11_GPIO,&value);
    while(value==IOT_GPIO_VALUE0 && retry<100)
    {
        IoTGpioGetInputVal(DHT11_GPIO,&value);
        retry++;
        hi_udelay(1);
    }
    if(retry>=100)return 1;
    return 0;
} 
uint8_t DHT11_Init(void)
{
    IoTGpioInit(DHT11_GPIO);
    IoSetFunc(DHT11_GPIO,IOT_IO_FUNC_GPIO_10_GPIO);
    DHT11_IO_OUT();
    IoTGpioSetOutputVal(DHT11_GPIO,IOT_GPIO_VALUE0);
    usleep(5000*1000);
    DHT11_RST();
    return DHT11_Check();
}
uint8_t DHT11_Read_bit(void)//读取一个位返回1/0
{
    uint8_t str;
    IotGpioValue value = IOT_GPIO_VALUE0;
    //获取GPIO10引脚状态
    uint8_t retry;
    IoTGpioGetInputVal(DHT11_GPIO,&str);
    while(value==IOT_GPIO_VALUE1 && retry<100)//等待总线变为低电平
    {
    IoTGpioGetInputVal(DHT11_GPIO,&value);
        
        retry++;
        hi_udelay(1);
    }
    printf("6666666666\r\n");
    retry=0;
    IoTGpioGetInputVal(DHT11_GPIO,&value);
    while(value==IOT_GPIO_VALUE0 && retry<100)//等待总线变为高电平
    {
        IoTGpioGetInputVal(DHT11_GPIO,&value);
        retry++;
        usleep(1);
        
    }
    printf("7777777777\r\n");
    usleep(40);//等待40us
    IoTGpioGetInputVal(DHT11_GPIO,&value);//判断当前的电平情况从而判断数据为0还是1
    printf("8888888888\r\n");
    if(value==IOT_GPIO_VALUE1)
    return 1;
    else
    return 0;
}
uint8_t DHT11_Read_Byte(void)
{
    uint8_t i,dat;
    dat=0;
    for(i=0;i<8;i++)
    {
        dat<<=1;
        dat|=DHT11_Read_bit();
    }
    printf("5555555555566665656565656\r\n");
    return dat;
}
uint8_t DHT11_Read_Data(uint8_t* h)//读取DHT11的数据返回数值1时代表读取失败  返回0读取成功
{
    uint8_t data[5];
    uint8_t i;
    DHT11_RST();
    printf("1111111111\r\n");
    if(DHT11_Check()==0)
    {
        for(i=0;i<5;i++)
        {
            printf("2222222222\r\n");
            data[i]=DHT11_Read_Byte();
            printf("data=%d\r\n",data[i]);
        }
        if((data[0]+data[1]+data[2]+data[3])==data[4])
        {
        printf("4444444444\r\n");
        *h=data[0];
        h=h+1;
        *h=data[2];
        printf("5555555555\r\n");
        }
    }else return 1;
    return 0;
} 
#endif