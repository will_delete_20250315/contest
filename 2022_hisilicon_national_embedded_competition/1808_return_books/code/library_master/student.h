#ifndef STUDENT_H
#define STUDENT_H
#include "QString"
#include "QList"
extern QFont font1,font2,font3,font4;
class Student
{
public:
    Student(QString id,QString name,QString picturePath,QString major,QString grade,QList<QString> borrowBookList);
    Student();
    QString id;//学生ID号
    QString name;//学生姓名
    QString picturePath;//学生图片路径
    QString major;//学生专业
    QString grade;//学生年级
    QList<QString> borrowBookList;//学生还的书的列表

    void setName(QString id);
    void setPicturePath(QString id);
};

#endif // STUDENT_H
