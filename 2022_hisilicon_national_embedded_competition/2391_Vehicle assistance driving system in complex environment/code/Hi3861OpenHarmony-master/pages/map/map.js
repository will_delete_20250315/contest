// pages/map/map.js
const app = getApp()
const APIKEY = "27702518129d4c40ba0a98c743bc5e28";// 填入你申请的KEY
Page({
  /**
   * 页面的初始数据
   */
  data: {
    addmissage: '选的位置',
    // markers	 Array	标记点
    stitle:'故宫',
    latitude: "",
    longitude: "",
    scale: 14,
    markers: [],
    //iot
    productId: app.globalData.productId,
    deviceName: app.globalData.deviceName,
    stateReported: {},
    now:"",
    statetext:"当前驾驶环境正常",
    textcolor:"#00c8b6",
    //iot

    //controls控件 是左下角圆圈小图标,用户无论放大多少,点这里可以立刻回到当前定位(控件（更新一下,即将废弃，建议使用 cover-view 代替）)
    controls: [{
      id: 1,
      iconPath: '../../images/img/controls.png',
      position: {
        left: 15,
        top: 260 - 50,
        width: 40,
        height: 40
      },
      clickable: true
    }],
    distanceArr: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取当前的地理位置、速度,
    //在这里改数据
    
    wx.getLocation({
      type: 'wgs84', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
      success: function (res) {
        //赋值经纬度
        that.setData({   //success里面不能用this
          latitude: res.latitude,
          longitude: res.longitude,

        })
      }
    })

    //天气
      var that = this
      // wx.showLoading({
      //   title: '加载中',
      // })

      //经纬度信息
      wx.request({
        url: 'https://devapi.qweather.com/v7/weather/now?key=' + APIKEY + "&location=" + "121,38",
        // stateReported.motor+","+stateReported.temperature,
        success(result) {
          var res = result.data
          // console.log(res)
          that.setData({
            now: res.now
          })
        }
      })


    //iot
    //iot


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.cloud.callFunction({
      name: 'iothub-shadow-query',
      data: {
        ProductId: app.globalData.productId,
        DeviceName: app.globalData.deviceName,
        SecretId: app.globalData.secretId,
        SecretKey: app.globalData.secretKey,
      },
      success: res => {
        // wx.showToast({
        //   icon: 'none',
        //   title: 'Subscribe完成，获取云端数据成功',
        // })

        let deviceData = JSON.parse(res.result.Data)

        //判断环境
        console.log(deviceData.payload.state.reported.light)
        if(deviceData.payload.state.reported.light==1){
          this.setData({
            statetext:"当前驾驶环境恶劣",
            textcolor: "red"
          })
        }
        else if(deviceData.payload.state.reported.light==0){
          this.setData({
            statetext:"当前驾驶环境正常",
            textcolor: "#00c8b6"
          })
        }
        this.setData({
          stateReported: deviceData.payload.state.reported
        })
        // console.log("result:", deviceData) //payload那一大串
        this.onShow()
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: 'Subscribe失败，获取云端数据失败',
        })
        console.error('[云函数] [iotexplorer] 调用失败：', err)
      }
    })
    // let obj = {
    //   ["light"]: 1
    // }
    // let payload = JSON.stringify(obj)

    // // wx.showLoading()
    // wx.cloud.callFunction({
    //   name: 'iothub-publish',
    //   data: {
    //     SecretId: app.globalData.secretId,
    //     SecretKey: app.globalData.secretKey,
    //     ProductId: app.globalData.productId,
    //     DeviceName: app.globalData.deviceName,
    //     Topic: app.globalData.productId + "/" + app.globalData.deviceName + "/data",
    //     Payload: payload,
    //   },
    //   success: res => {
    //     // wx.showToast({
    //     //   icon: 'none',
    //     //   title: 'publish完成',
    //     // })
    //     // console.log("res:", res)  //打印ID
    //   },
    //   fail: err => {
    //     wx.showToast({
    //       icon: 'none',
    //       title: 'publish失败，请连接设备',
    //     })
    //     console.error('[云函数] [iotexplorer] 调用失败：', err)
    //   }
    // })  

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})