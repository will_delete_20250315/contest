

#ifndef __TANK_CONTROL_H__
#define __TANK_CONTROL_H__


#define     TANK_CONTROL_DEMO_TASK_STAK_SIZE   (1024*10)
#define     TANK_CONTROL_DEMO_TASK_PRIORITY    (25)
#define     SHECHENG                            (20)
#define     KEY_INTERRUPT_PROTECT_TIME        (30)
#define     TANK_GO_LEFT                     (0)
#define     TANK_GO_RIGHT                    (1)
#define     ADC_TEST_LENGTH                   (20)


typedef enum   //自定义枚举
{
    TANK_RECONNOITRE_STATUS = 0,  //坦克侦察状态
    TANK_STOP_SHOOT_STATUS,      //坦克停止并射击状态
} TankStatus;


void switch_init(void);
void interrupt_monitor(void);
unsigned char GetCarStatus(void);

//custome
extern int g_tank_status; //坦克初始侦察状态
void tank_mode_control(void);
int tank_reconnoitre(void);

#endif
