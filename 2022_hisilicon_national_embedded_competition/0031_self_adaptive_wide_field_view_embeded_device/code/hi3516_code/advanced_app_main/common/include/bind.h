#ifndef __COMMON_BIND_H__
#define __COMMON_BIND_H__

#include "hi_types.h"
#include "hi_common.h"

HI_S32 VI_Bind_VPSS(VI_PIPE ViPipe, VI_CHN ViChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn);

HI_VOID VI_UnBind_VPSS(VI_PIPE ViPipe, VI_CHN ViChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn);

HI_S32 VPSS_Bind_VO(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn);

HI_VOID VPSS_UnBind_VO(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn);

#endif