#ifndef __COMMON_VO_H__
#define __COMMON_VO_H__

#include "hi_types.h"
#include "hi_common.h"

HI_S32 VO_Init(VO_DEV VoDev, VO_LAYER VoLayer, VO_CHN VoChn);

HI_VOID VO_DeInit(VO_DEV VoDev, VO_LAYER VoLayer, VO_CHN VoChn);

#endif