#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"

#include "hi_io.h" //==hi_io_set_func()、hi_io_set_pull()
#include "hi_uart.h"

#include "iot_uart.h"
#include "bluetooth.h"
int Bluetooth_Init()
{
    hi_io_set_func(BLUE_RX_1, HI_IO_FUNC_GPIO_1_UART1_RXD);
    hi_io_set_func(BLUE_TX_0, HI_IO_FUNC_GPIO_0_UART1_TXD);

    hi_uart_attribute my_uart1;
    my_uart1.baud_rate = 115200;
    my_uart1.data_bits = 8;
    // my_uart1.pad = 0;
    my_uart1.parity = HI_UART_PARITY_NONE;
    my_uart1.stop_bits = 1;

    hi_uart_extra_attr my_extra;
    // my_extra.flow_fifo_line = 0;
    my_extra.rx_block = HI_UART_BLOCK_STATE_NONE_BLOCK;
    // my_extra.rx_buf_size = 1;
    // my_extra.rx_fifo_line = 1;
    // my_extra.rx_use_dma = 1;
    my_extra.tx_block = HI_UART_BLOCK_STATE_NONE_BLOCK;
    // my_extra.tx_buf_size = 1;
    // my_extra.tx_fifo_line = 1;
    // my_extra.tx_use_dma = 1;

    //iot封装
    // IotUartAttribute my_param;//={115200,8,1,HI_UART_PARITY_NONE,0,0};
    //  /** Baud rate */
    // my_param.baudRate=115200;
    // /** Data bits */
    // my_param.dataBits=8;
    // /** Stop bit */
    // my_param.stopBits=1;
    // /** Parity */
    // my_param.parity=HI_UART_PARITY_NONE;
    // /** Rx block state */
    // my_param.rxBlock=0;
    // /** Tx block state */
    // my_param.txBlock=0;
    // /** Padding bit */
    // my_param.pad=0;
    // IoTUartInit(HI_UART_IDX_1, &my_param);
    return hi_uart_init(HI_UART_IDX_1, &my_uart1, NULL);
}

int Bluetooth_read(char *get_data, int data_len)
{
    if(get_data ==NULL)
    {
        return -1;
    }   
    int ret;
    // ret = hi_uart_read(HI_UART_IDX_1, get_data, data_len);
    ret=hi_uart_read_timeout(HI_UART_IDX_1, get_data, data_len, 10); //限时读取 防堵塞
    return ret;

}
int  Bluetooth_write(char *get_data, int data_len)
{
     if(get_data ==NULL)
    {
        return -1;
    }
    int ret;
    ret = hi_uart_write(HI_UART_IDX_1, get_data, data_len);
    return ret;
}


/*  
//测试蓝牙
static void BlueTask(void *arg)
{
    (void)arg;
    sleep(2);
    int ret = 1;
    ret = Bluetooth_Init();
    if (ret != 0)
    {
        printf("Uart1 init failed! \n");
        // return;
    }
    char send_data[] = "AB";
    char get_data[8] = {0};
    printf("get_data=%s\n", send_data);

    while (1)
    {

        usleep(500000); // 0.5s
        ret = hi_uart_read(HI_UART_IDX_1, get_data, 8);
        if (ret == 0)
            printf("get_data = %s\n", get_data);
        else
            printf("get_data Falile\r\n");
    }
}



static void BlueDemo(void)
{
    osThreadAttr_t attr;

    attr.name = "BlueTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;

    if (osThreadNew(BlueTask, NULL, &attr) == NULL)
    {
        printf("[BlueDemo] Falied to create BluetoothTask!\n");
    }
}

APP_FEATURE_INIT(BlueDemo);


*/