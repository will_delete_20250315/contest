/********************************************************************
	created:	2010/11/15
	created:	15:11:2010   16:09
	filename: 	BWLabel.C
	author:		Hu Jun
	
	purpose:	Label BW Image Regions, 
				Like the Function 'bwlabel' in MATLAB
*********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "BWLabel.h"
#include "Stack.h"


//STACK Stack={0};
/*
void BWLabel_init()
{
	if(CreateStack(&Stack)!=OK)//初始化堆栈
		exit(-1);
}

void BWLabel_end()
{
	DestroyStack( &Stack );
}
*/

/************************************************************************/
/* 
函数：BWLabel
功能：实现二值图像的连通区域标记
参数：
	pnBW---指向二值图像数据，要求像素值为0、1
	pnBWLabel---指向标记后的图像
	nImageWidth、nImageHeight---图像高宽
	nMode---邻域的连接模式：8为八连通，4为四连通

返回：图像中区域数目
*/


/************************************************************************/
int BWLabel( unsigned char *pnBW, int nImageWidth, int nImageHeight, int nMode, s_bw_Line_Table *p_line_table, s_bw_Block_Table *p_block_table)
{
	int i=0, j=0, k=0, m=0, n=0, L=0, R=0, LL=0, RR=0,  X=0, nFlag=0;
	unsigned char *pnBWPadded=NULL;
	int nWidthPadded=nImageWidth+2, nHeightPadded=nImageHeight+2;
	int *pnSegNumPerLine=NULL, **pnSegments=NULL, *pnSegmentTemp=NULL;

	int N;
	int line_index,	block_index;
	int tmp_xLeft, tmp_xRight;			// 区块左右x边界
	int tmp_yTop, tmp_yBottom;			// 区块上下y边界
	int tmp_area;					    // 区块面积
	int tmp_centroid_X,tmp_centroid_Y;	// 区域质心坐标
	//printf("[ %d ]BWLabel in....\n",index);
	STACK Stack={0};

	if ( nMode!=CONNECTIVITY4 && nMode!=CONNECTIVITY8 )
	{
		printf("必须指定nMode的值，必须是8或者4！");
		exit(-1);
	}

	//memset(pnBWLabel,0,nImageWidth*nImageHeight);

	//加边，补零
	pnBWPadded = (unsigned char *)calloc( nWidthPadded*nHeightPadded, sizeof(unsigned char) );
	for ( i=0; i<nImageHeight; i++ )
		memcpy( pnBWPadded+(i+1)*nWidthPadded+1, pnBW+i*nImageWidth, nImageWidth*sizeof(unsigned char) );

	//统计每一行线段的数目，存入pnLineNumber
	pnSegNumPerLine = (int *)calloc( nHeightPadded, sizeof(int) );
	for ( i=0; i<nImageHeight; i++ )
		for ( j=0; j<nWidthPadded-1; j++ )
		{
			if ( pnBWPadded[(i+1)*nWidthPadded+j]==0 && pnBWPadded[(i+1)*nWidthPadded+j+1]==255 )
				pnSegNumPerLine[i+1]++;
		}

	//为每一行的线段端点存储分配空间
	pnSegments = (int **)calloc( nHeightPadded, sizeof(int *) );
	for ( i=0; i<nHeightPadded; i++ )
		pnSegments[i] = (int *)calloc( pnSegNumPerLine[i]*4, sizeof(int) );

	//扫描标记
	/*pnSegment矩阵中的数据结构*/
	/*---------------------------
	  |行号|左端点|右端点|标志位|
	  ---------------------------
	  |行号|左端点|右端点|标志位|
	  ---------------------------
	  |行号|左端点|右端点|标志位|
	  ---------------------------
	  |行号|左端点|右端点|标志位|
	  ---------------------------*/
	for ( i=1; i<nHeightPadded-1; i++ )
	{
		n = 0;
		for ( j=0; j<nWidthPadded-1; j++ )
		{
			if ( pnBWPadded[i*nWidthPadded+j]==255 )
			{
				for ( k=j+1; k<nWidthPadded; k++ )
				{
					if ( pnBWPadded[i*nWidthPadded+k]==0 )
						break;
				}
				pnSegments[i][n*4  ] = i;  //记录行号
				pnSegments[i][n*4+1] = j;  //记录左端点
				pnSegments[i][n*4+2] = k-1;//记录右端点
				n++;
				j = k;
			}
		}
	}

	if(CreateStack(&Stack)!=OK)//初始化堆栈
		exit(-1);

	N = 1;//区域的编号

	line_index = 0;
	block_index = 0;

	//初始化第一个区域的起始lineIndex
	p_block_table[block_index].lineTable1 = line_index;
	
	tmp_xLeft=nImageWidth, tmp_xRight=0;			// 区块左右x边界
	tmp_yTop=nImageHeight, tmp_yBottom=0;			// 区块上下y边界
	tmp_area=0;					    // 区块面积
	tmp_centroid_X=0,tmp_centroid_Y=0;	// 区域质心坐标

	for ( i=1; i<nHeightPadded-1; i++ )
	{
		for ( j=0; j<pnSegNumPerLine[i]; j++ )
		{
			//线段未被标记且未被扫描，即新的一段
			if ( pnSegments[i][j*4+3]==0 )
			{
				pnSegmentTemp = &pnSegments[i][j*4];
				pnSegmentTemp[3] = -1;//标志位置-1表示入栈
				if (Push(&Stack, pnSegmentTemp)!=OK)//入栈
					exit(-1);
Loop:	
				if (GetTopElement(Stack, &pnSegmentTemp)!=OK)//获取栈顶元素
					exit(-1);

				X = pnSegmentTemp[0];//行号
				L = pnSegmentTemp[1];//左端点
				R = pnSegmentTemp[2];//右端点

				//区分是八连通还是四连通
				switch ( nMode )
				{
				case CONNECTIVITY8:
					LL = L-1, RR = R+1;
					break;
				case CONNECTIVITY4:
					LL = L, RR = R;
					break;
				default:
					printf("必须指定nMode的值，必须是8或者4！");
					exit(-1);
				}
				
				nFlag = 0;
				//扫描上一行看是否存在未标记的邻接线段，存在，nFlag=1，将邻接段压入堆栈
				for ( m=0; m<pnSegNumPerLine[X-1]; m++ )
				{
					if ( pnSegments[X-1][m*4+1]<=RR && pnSegments[X-1][m*4+2]>=LL && pnSegments[X-1][m*4+3]==0 )
					{
						pnSegments[X-1][m*4+3] = -1;
						if (Push(&Stack, &pnSegments[X-1][m*4])!=OK)
							exit(-1);
						nFlag = 1;
					}
				}
				//扫描下一行看是否存在邻接线段，存在，nFlag=1，将邻接段压入堆栈
				for ( n=0; n<pnSegNumPerLine[X+1]; n++ )
				{
					if ( pnSegments[X+1][n*4+1]<=RR && pnSegments[X+1][n*4+2]>=LL && pnSegments[X+1][n*4+3]==0 )
					{
						pnSegments[X+1][n*4+3] = -1;
						if (Push(&Stack, &pnSegments[X+1][n*4])!=OK)
							exit(-1);
						nFlag = 1;
					}
				}

				//表明该段邻接段均已标记
				//或者不存在邻接段
				if ( nFlag==0 )
				{
					if ( IsStackEmpty(Stack)==TRUE )//栈为空，继续标记下一个连通区域
					{
						//如果区域数超限，则忽略后续处理
						if (block_index>=MAX_BW_BLOCK)
						{
							continue;
						}
						
						//记录区域信息
						p_block_table[block_index].xLeft = tmp_xLeft;
						p_block_table[block_index].xRight = tmp_xRight;
						p_block_table[block_index].yTop = tmp_yTop;
						p_block_table[block_index].yBottom = tmp_yBottom;

						p_block_table[block_index].area = tmp_area;
						if (tmp_area)//预防零除
						{
							p_block_table[block_index].centroid_X = tmp_centroid_X/tmp_area;
							p_block_table[block_index].centroid_Y = tmp_centroid_Y/tmp_area;
						}
						

						//记录线段表下边界
						p_block_table[block_index].lineTable2 = line_index-1;

						//
						N++;
						block_index++;
						
						//初始化新区域线段表上边界
						p_block_table[block_index].lineTable1 = line_index;

						//初始化临时变量
						tmp_xLeft=nImageWidth, tmp_xRight=0;			// 区块左右x边界
						tmp_yTop=nImageHeight, tmp_yBottom=0;			// 区块上下y边界
						tmp_area=0;					    // 区块面积
						tmp_centroid_X=0,tmp_centroid_Y=0;	// 区域质心坐标

						continue;
					}
					else
					{
						if (line_index<MAX_BW_LINE)
						{
							Stack.pArray[Stack.nTopElement][3] = N;//栈为不空，标记当前连通区域  //标记当前行

							//记录当前行
							p_line_table[line_index].xLeft = Stack.pArray[Stack.nTopElement][1]-1;
							p_line_table[line_index].xRight = Stack.pArray[Stack.nTopElement][2]-1;
							p_line_table[line_index].yLine = Stack.pArray[Stack.nTopElement][0]-1;

							//更新区域边界
							tmp_xLeft   = ( p_line_table[line_index].xLeft <tmp_xLeft   ) ? p_line_table[line_index].xLeft  :tmp_xLeft;
							tmp_xRight  = ( p_line_table[line_index].xRight >tmp_xRight ) ? p_line_table[line_index].xRight :tmp_xRight;
							tmp_yTop    = ( p_line_table[line_index].yLine <tmp_yTop    ) ? p_line_table[line_index].yLine  :tmp_yTop;
							tmp_yBottom = ( p_line_table[line_index].yLine >tmp_yBottom ) ? p_line_table[line_index].yLine  :tmp_yBottom;

							//累加面积、质心
							for (int x=(Stack.pArray[Stack.nTopElement][1]-1);x<=(Stack.pArray[Stack.nTopElement][2]-1);++x)
							{
								tmp_area++;
								tmp_centroid_X += x;
								tmp_centroid_Y +=(Stack.pArray[Stack.nTopElement][0]-1);
							}


							line_index++;
						}					


						if (Pop(&Stack, &pnSegmentTemp)!=OK)//将其从堆栈中弹出
							exit(-1);

						//栈为空，表明当前连通区域标记完毕
						//开始下一个连通区域的标记
						if ( IsStackEmpty( Stack )==TRUE )
						{
							//如果区域数超限，则忽略后续处理
							if (block_index>=MAX_BW_BLOCK)
							{
								continue;
							}

							//记录区域信息
							p_block_table[block_index].xLeft = tmp_xLeft;
							p_block_table[block_index].xRight = tmp_xRight;
							p_block_table[block_index].yTop = tmp_yTop;
							p_block_table[block_index].yBottom = tmp_yBottom;

							p_block_table[block_index].area = tmp_area;
							if (tmp_area)//预防零除
							{
								p_block_table[block_index].centroid_X = tmp_centroid_X/tmp_area;
								p_block_table[block_index].centroid_Y = tmp_centroid_Y/tmp_area;
							}
							//记录线段表下边界
							p_block_table[block_index].lineTable2 = line_index-1;

							//
							N++;
							block_index++;

							//初始化新区域线段表上边界
							p_block_table[block_index].lineTable1 = line_index;

							//初始化临时变量
							tmp_xLeft=nImageWidth, tmp_xRight=0;			// 区块左右x边界
							tmp_yTop=nImageHeight, tmp_yBottom=0;			// 区块上下y边界
							tmp_area=0;					    // 区块面积
							tmp_centroid_X=0,tmp_centroid_Y=0;	// 区域质心坐标

							continue;
						}
						else
							goto Loop;//栈不为空，继续处理栈顶元素
					}
				}
				else
					goto Loop;//继续处理栈顶元素
			}
		}
	}

	//依据pnSegments中各个标记位，标记二值图像
	/*for ( i=0; i<nImageHeight; i++ )
		for ( j=0; j<pnSegNumPerLine[i+1]; j++ )
			for ( k=pnSegments[i+1][j*4+1]; k<=pnSegments[i+1][j*4+2]; k++ )
				pnBWLabel[i*nImageWidth+k-1] = pnSegments[i+1][j*4+3];*/

	free( pnBWPadded ), pnBWPadded = NULL;
	free( pnSegNumPerLine ), pnSegNumPerLine = NULL;
	for ( i=0; i<nHeightPadded; i++ )
	{
		if ( pnSegments[i]!=NULL )
			free(pnSegments[i]), pnSegments[i]=NULL;
	}
	free(pnSegments), pnSegments=NULL;
	DestroyStack( &Stack );
		
	return N-1;
}
