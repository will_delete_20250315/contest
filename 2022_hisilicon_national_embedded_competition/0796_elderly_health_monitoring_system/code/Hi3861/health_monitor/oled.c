/*
 * Copyright (c) 2022 HiSilicon (Shanghai) Technologies CO., LIMITED.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_i2c.h"
#include "iot_gpio.h"
#include "iot_errno.h"

#include "oled_ssd1306.h"

#include "task_start.h"
#include "max30102_func.h"
#include "orientation.h"


#define AHT20_BAUDRATE (400 * 1000)
#define AHT20_I2C_IDX 0

static void OledmentTask(const char *arg)
{      
    (void)arg;
    OledInit();
    OledFillScreen(0);
    IoTI2cInit(AHT20_I2C_IDX, AHT20_BAUDRATE);
    char HeartRate[10];
    char BloodOxygen[3];
    //BloodOxygen[3]='%';
    //char Latitude[10];
    //char longitude[10];
    
    int O2_flag,HR_flag;

    while(1)
    { 

        sprintf(HeartRate,"%d",n_heart_rate);
        sprintf(BloodOxygen,"%d",n_sp02);

        if(n_sp02<=94)//0为异常
        O2_flag=0;
        else 
        O2_flag=1;

        if(n_heart_rate>=50&&n_heart_rate<=300) 
        HR_flag=1;
        else 
        HR_flag=0;

        printf("O2_flag=%d,HR_flag=%d\n",O2_flag,HR_flag);
        if(BloodOxygen[0]==1)
         BloodOxygen[2]='0';
        //else if(BloodOxygen[0]=='-')
        // {BloodOxygen[0]='x';BloodOxygen[1]='x';BloodOxygen[2]='x';}
        else
         BloodOxygen[2]=0;
        
        //printf("oled n_sp02=%d  %i \n",n_sp02,n_sp02);
        //printf("BloodOxygen[0]=%c  BloodOxygen[1]=%c  BloodOxygen[2]=%c  \n",BloodOxygen[0],BloodOxygen[1],BloodOxygen[2]);
       

        OledShowString(1, 1,"HeartRate ",1);        OledShowString(90, 1, HeartRate, 1); /* 屏幕第1列1行显示1行 */
        OledShowString(1, 2, "BloodOxygen", 1);  OledShowString(90, 2, BloodOxygen, 1); /* 屏幕第1列1行显示1行 */
        OledShowString(1, 3, "Latitude       ", 1); OledShowString(90, 3, Save_Data.latitude, 1); /* 屏幕第1列1行显示1行 */
        OledShowString(1, 4, "longitude      ", 1); OledShowString(90, 4, Save_Data.longitude, 1); /* 屏幕第1列1行显示1行 */

        if(Save_Data.isUsefull==0)
        OledShowString(1, 5, "location failed", 1);
        else if(Save_Data.isUsefull==1)
        OledShowString(1, 5, "location succeed", 1);
        
        if(ex_Range<=100)
        OledShowString(1, 6, " Have to wear ", 1);
        else if(ex_Range>100)
        OledShowString(1, 6, " Not to wear ", 1);
        msleep(1000);


        if(O2_flag==1&&HR_flag==1)
        OledShowString(1, 7, "Normal BO HR", 1);

        if(O2_flag==0&&HR_flag==0)
        OledShowString(1, 7, "Abnormal BO HR", 1);

        if(O2_flag==1&&HR_flag==0)
        OledShowString(1, 7, "Abnormal HR   ", 1);

        if(O2_flag==0&&HR_flag==1)
        OledShowString(1, 7, "Abnormal O2   ", 1);


    }
}

static void OledDemo(void)
{
    osThreadAttr_t attr;
    attr.name = "OledmentTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096; /* 任务大小4096 */
    attr.priority = osPriorityNormal;

    if (osThreadNew(OledmentTask, NULL, &attr) == NULL) {
        printf("[OledDemo] Falied to create OledmentTask!\n");
    }
}

APP_FEATURE_INIT(OledDemo);


void time()
{
    


}