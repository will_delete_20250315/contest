
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include <ohos_init.h>
#include <cmsis_os2.h>
#include <iot_i2c.h>
#include <iot_errno.h>
#include <hi_uart.h>
#include <hi_io.h>
#include "orientation.h"
#include <iot_gpio_ex.h>
#include <hi_task.h>

#define USART_REC_LEN 200  	//??????????????? 200
#define UART_BUFF_SIZE           20*16

_SaveData Save_Data;




void GPS_init(void)
{
   hi_uart_attribute uart_attr = {
   .baud_rate = BAUDRATE,
   .data_bits = 8,
   .stop_bits = 1,
   .parity = 0
	};

	IoSetFunc(HI_IO_NAME_GPIO_0, IOT_IO_FUNC_GPIO_0_UART1_TXD);
    IoSetFunc(HI_IO_NAME_GPIO_1, IOT_IO_FUNC_GPIO_1_UART1_RXD);
	
	int ret;
	ret = hi_uart_init(UART_NUM, &uart_attr,HI_NULL);
	if(ret==0)
		printf("UartInit successful!\n");
	else 
		printf("UartInit failed\n");

	clrStruct();
}



void parseGpsBuffer()
{
	char *subString;
	char *subStringNext;
	char i = 0;
	if (Save_Data.isGetData)
	{
		Save_Data.isGetData = false;
		printf("**************\r\n");
		printf(Save_Data.GPS_Buffer);	
		for (i = 0 ; i <= 6 ; i++)
		{
			if (i == 0)
			{
				if ((subString = strstr(Save_Data.GPS_Buffer, ",")) == NULL)
					errorLog(1);	//????????
			}
			else
			{
				subString++;
				if ((subStringNext = strstr(subString, ",")) != NULL)
				{
					char usefullBuffer[2]; 
					switch(i)
					{
						case 1:memcpy(Save_Data.UTCTime, subString, subStringNext - subString);break;	//???UTC???
						case 2:memcpy(usefullBuffer, subString, subStringNext - subString);break;	//???UTC???
						case 3:memcpy(Save_Data.latitude, subString, subStringNext - subString);break;	//???��?????
						case 4:memcpy(Save_Data.N_S, subString, subStringNext - subString);break;	//???N/S
						case 5:memcpy(Save_Data.longitude, subString, subStringNext - subString);break;	//??????????
						case 6:memcpy(Save_Data.E_W, subString, subStringNext - subString);break;	//???E/W
						default:break;
					}

					subString = subStringNext;
					Save_Data.isParseData = true;
					if(usefullBuffer[0] == 'A')
						Save_Data.isUsefull = true;
					else if(usefullBuffer[0] == 'V')
						Save_Data.isUsefull = false;
				}
				else
				{
					errorLog(2);	//????????
				}
			}


		}
	}
}

//???GPS???
void printGpsBuffer()
{
	if (Save_Data.isParseData)
	{
		Save_Data.isParseData = false;
		
		printf("UTCTime = ");
		printf(Save_Data.UTCTime);
		printf("\r\n");

		if(Save_Data.isUsefull)
		{
			Save_Data.isUsefull = false;
			printf("latitude = ");
			printf(Save_Data.latitude);
			printf("\r\n");


			printf("N_S = ");
			printf(Save_Data.N_S);
			printf("\r\n");

			printf("longitude = ");
			printf(Save_Data.longitude);
			printf("\r\n");

			printf("E_W = ");
			printf(Save_Data.E_W);
			printf("\r\n");
		}
		else
		{
			printf("GPS DATA is not usefull!\r\n");
		}
		
	}
}

void errorLog(int num)
{
	
	while (1)
	{
	  	printf("ERROR%d\r\n",num);
	}
}


void clrStruct(void)
{
	Save_Data.isGetData = false;
	Save_Data.isParseData = false;
	Save_Data.isUsefull = false;
	memset(Save_Data.GPS_Buffer, 0, GPS_Buffer_Length);      //???
	memset(Save_Data.UTCTime, 0, UTCTime_Length);
	memset(Save_Data.latitude, 0, latitude_Length);
	memset(Save_Data.N_S, 0, N_S_Length);
	memset(Save_Data.longitude, 0, longitude_Length);
	memset(Save_Data.E_W, 0, E_W_Length);
}

hi_u16 point1 = 0;
hi_u8 USART_RX_BUF[UART_BUFF_SIZE] = {0};
hi_u8 *uart_buff_ptr = USART_RX_BUF;

//hi_u8* Res_ptr;
void GPS_Read(void)                	//????1?��???????
{
	//printf("in read1\n");
	hi_u8 save_ptr[50];
	memset(Save_Data.GPS_Buffer, 0, GPS_Buffer_Length); 
	//save_ptr=Save_Data.GPS_Buffer;
    uint32_t len=0; //=hi_uart_read(UART_NUM, USART_RX_BUF, UART_BUFF_SIZE);
	for(int i=0;i<20;i++)
	{
		hi_uart_read(UART_NUM,(USART_RX_BUF+i*16), UART_BUFF_SIZE);//该函数一次最多只能读16个字节
		 //printf("i=%d\n",i);
		 msleep(1);
	}
	
	//printf("USART_RX_BUF=\n%s\n",USART_RX_BUF);   
	
	//printf("len=%d\n",len);   
    //printf("uart_buff_ptr=%s\n",uart_buff_ptr);  
	//if (len > 0) 
    //hi_uart_write(0, uart_buff_ptr, len);
	  
    int index=FindStr(USART_RX_BUF,"GNRMC");      
    //printf("index=%d\r\n",index);
	
	int max_enter;
	int j;
	for(j=0;USART_RX_BUF[j]!='\0';j++)
	{
		
		if(USART_RX_BUF[j]=='\n')
		{
			max_enter=j;
			//printf("j=%d\n",j);

		}
	}
    //printf("max_enter=%d\r\n",max_enter);
	if(index<=max_enter)
	{	
		int k=0;
		for(j=index-2;USART_RX_BUF[j]!='\n';j++,k++)
		{
			save_ptr[k]=USART_RX_BUF[j];
			//printf("j=%d  ",j);	
		}
	}
	//printf("\n");
	if(index<=max_enter)
	{
		strcpy(Save_Data.GPS_Buffer,save_ptr);
		//printf("save_ptr=%s\n",save_ptr);
		//printf("GPS_Buffer=%s\n",Save_Data.GPS_Buffer);
		Save_Data.isGetData = true;
	}
	
	//if(*USART_RX_BUF=='G'&& *(USART_RX_BUF+4)=='C')
	//{
    //   for(int i=0;USART_RX_BUF[i]!='\0';i++)
    //   {
    //   *save_ptr=USART_RX_BUF[i];
    //   save_ptr++;
	//    //printf("i=%d ",i);
    //   }
	//	//printf("\n");
	//}
	
    //printf("Save_Data.GPS_Buffer=%s\n",Save_Data.GPS_Buffer);

    memset(USART_RX_BUF, 0, UART_BUFF_SIZE);
	
}   


hi_u16 FindStr(char *str,char *ptr)
{
    //printf("in find\n");		
	hi_u16 index=0;
	char *STemp=NULL;
	char *PTemp=NULL;
	char *MTemp=NULL;
	if(0==str||0==ptr)
		return 0;
	for(STemp=str;*STemp!='\0';STemp++)	 //���β����ַ���
	{
		index++;  //��ǰƫ������1
		MTemp=STemp; //ָ��ǰ�ַ���
		//�Ƚ�
		for(PTemp=ptr;*PTemp!='\0';PTemp++)
		{	
			if(*PTemp!=*MTemp)
			break;
			MTemp++;
		}
		if(*PTemp=='\0')  //��������Ҫ���ҵ��ַ����˳�
			break;
	}
	return index;

	    //printf(len);
	//	for(int i=0;i<=UART_BUFF_SIZE;i++)
	//	{
    //        printf("in find");		
	//		if(str[i]=='$' && str[i+4] == 'M' && str[i+5] == 'C')
	//			{for(int j=i;str[j]!='\n';j++) 
	//			    { *ptr=str[j];
	//				  ptr++;
	//				}
	//			break;
	//			}			
	//	}				   
    //    Save_Data.isGetData = true;
	//	memset(str, 0, UART_BUFF_SIZE);    

}
