/*
 * Copyright (C) 2022 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
  */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"
#include "hi_time.h"

//下面三个引脚控制舵机
#define GPIO0 3 //上1
#define GPIO1 8 //上2
#define GPIO2 2 //上3

#define COUNT 10
void set_angle_0(unsigned int duty)
{
    unsigned int time = 20000;
    // IoTGpioInit(GPIO0);
    // IoSetFunc(GPIO0,0);
    // IoTGpioSetDir(GPIO0, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO0, IOT_GPIO_VALUE0);
    hi_udelay(time - duty);
}

void set_angle_1(unsigned int duty)
{
    unsigned int time = 20000;
    // IoTGpioInit(GPIO1);
    // IoSetFunc(GPIO1,0);
    // IoTGpioSetDir(GPIO1, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO1, IOT_GPIO_VALUE0);
    hi_udelay(time - duty);
}

void set_angle_2(unsigned int duty)
{
    unsigned int time = 20000;
    // IoTGpioInit(GPIO2);
    // IoSetFunc(GPIO2,0);
    // IoTGpioSetDir(GPIO2, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE1);
    hi_udelay(duty);
    IoTGpioSetOutputVal(GPIO2, IOT_GPIO_VALUE0);
    hi_udelay(time - duty);
}

// 500 1150 1800  -90度 0度 90度
//上1舵机 准确控制电机角度  输入参数：所想要偏转的角度 范围：-90~90°
void engine_turn1(int angle)
{
    int angle_pwm = 0;
    angle_pwm = angle * 7.22; //转换为一个周期的高电平时间
    //输出PWM波
    // for (int i = 0; i < COUNT; i++) {
    set_angle_0(angle_pwm + 1150);
    // }
}

//上2舵机 准确控制电机角度  输入参数：所想要偏转的角度 范围：-90~90°
void engine_turn2(int angle)
{
    int angle_pwm = 0;
    angle_pwm = angle * 7.22; //转换为一个周期的高电平时间
    //输出PWM波
    // for (int i = 0; i < COUNT; i++) {
    set_angle_1(angle_pwm + 1150);
    // }
}

// //上3舵机 准确控制电机角度  输入参数：所想要偏转的角度 范围：-90~90°
// void engine_turn3(int angle)
// {
//     int angle_pwm = 0;
//     angle_pwm = angle * 7.22; //转换为一个周期的高电平时间
//     //输出PWM波
//     for (int i = 0; i < COUNT; i++) {
//     set_angle_2(angle_pwm + 1150);
//     }
// }

void engine_turn_left(void)
{
    unsigned int angle = 500;
    for (int i = 0; i < COUNT; i++) {
        set_angle_0(angle);
    }
}


void engine_turn_right(void)
{
    unsigned int angle = 1150;
    for (int i = 0; i < COUNT; i++) {
        set_angle_0(angle);
    }
}


void regress_middle(void)
{
    unsigned int angle = 1800;
    for (int i = 0; i < COUNT; i++) {
        set_angle_0(angle);
    }
}
