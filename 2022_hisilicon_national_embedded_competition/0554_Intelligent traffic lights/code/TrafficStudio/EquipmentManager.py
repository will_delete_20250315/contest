

class Equipment(object):
    def __init__(self, equipId, workStatus , ak, sk, deviceId):
        self.equipId = equipId
        self.__workStatus = workStatus  # 设备是否能正常工作/是否存在
        self.__ak = ak
        self.__sk = sk
        self.__deviceId = deviceId
        self.__infoFlag = False # 标记当前对象中是否存储了信息
        self.__onlineStatus = None
        self.__updateTime = None
        self.__crossInfo = dict()

    def setInfoFlag(self, flag):
        self.__infoFlag = flag

    def getInfoFlag(self):
        return self.__infoFlag

    def setOnlineStatus(self, status):
        self.__onlineStatus = status

    def getOnlineStatus(self):
        return self.__onlineStatus

    def setUpdateTime(self, time):
        self.__updateTime = time

    def setCrossInfo(self, info):
        self.__crossInfo = info

    def getCrossInfo(self):
        return self.__crossInfo

    def getWorkStatus(self):
        return self.__workStatus

    def getLoginInfo(self):
        return self.__ak, self.__sk, self.__deviceId

    def getCongestionInfo(self):
        try:
            congestion = self.__crossInfo["current_congestion"]
            return True, congestion
        except:
            return False, []
