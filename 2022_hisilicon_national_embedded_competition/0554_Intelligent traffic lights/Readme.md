本设计利用Taurus AI视觉平台对路口交通拥堵状况进行分析，评估拥堵等级，
再通过Pegasus WiFi IoT平台将交通信息发送至华为云，
用户可以通过PC端应用程序查看交通信息和下发交通灯控制指令。
Hi3861也可以根据Hi3516的识别结果智能调整交通灯时长。
在实际应用中也可以引入AI对多个路口的拥堵信息进行分析，实现对全局交通的智能调控。

Pegasus
“app_demo_config.c” 关于红绿灯、数码管显示；倒计时的定时器；自动控制模式
“app_demo_iot.c”    远程控制模式；交通灯状态信息上报；云端信息解读
“hisignalling_protocol.c”   UART1串口互联；接收并处理上传Hi3516的信息

Taurus
## 1. 概述
我们在Hi3516DV300 SDK的基础之上进行开发，实现了车辆检测代码，车道检测代码，主要基于训练好的wk模型在板端进行部署，并充分发挥海思IVE、NNIE硬件加速能力，完成AI推理和业务处理。
## 2. 目录
my_smp：ai sample主入口及媒体处理文件
my_scenario/car_detect：主要用来封装车辆检测代码，以及实现上层功能。
my_scenario/my_yolo_process：主要用来实现YOLOv3在Hi3516DV300上的部署。
my_region_manage：车道范围推理的主要代码。
my_interconnection_server：串口通信的代码，更改了官方的串口发送接口。

TrafficStudio（上位机）
使用Pyqt开发