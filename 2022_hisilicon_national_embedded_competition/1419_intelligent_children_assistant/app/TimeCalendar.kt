package com.tangjinghao.monitor.util

import java.util.*

/**
 * @Author 唐靖豪
 * @Date 2022/7/13 15:48
 * @Email 762795632@qq.com
 * @Description
 */

object TimeCalendar {
    fun getTime(): String {
        val cal = Calendar.getInstance()
        val hour = cal.get(Calendar.HOUR_OF_DAY)
        var minute = cal.get(Calendar.MINUTE)
        if(minute<10){
            val str=0+minute
            return "$hour：$str"
        }else{
            return "$hour：$minute"
        }
    }
}