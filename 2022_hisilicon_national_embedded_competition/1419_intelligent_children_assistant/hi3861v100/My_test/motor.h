#ifndef __MOTOR_H__
#define __MOTOR_H__

#include <stdio.h>
#include <unistd.h>
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "hi_io.h"

#define COUNT 2

#define MOTOR_TASK_STACK_SIZE 1024 * 10

#define M1_GPIO 4
#define M2_GPIO 5
#define M3_GPIO 3
#define M4_GPIO 11
#define MPWM_GPIO 10
#define CPWM_GPIO 8

void move_forward();
void move_back();
void move_angle(float angle);
void move_stop();
void set_speed(unsigned int speed);
void Motor_GpioInit();

#endif // !__MOTOR_H__

