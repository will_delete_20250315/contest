#include "electronic_fence.h"


/*中值滤波*/
int medianFilter(int *buffer, int num);

/*tips:该函数请在查询此时有几个检测值后调用
param:边界框的左上角坐标、右下角坐标；物体的中心坐标
return:1越界；0未越界
*/
int fenceJudge(int fence_leftUp_x, int fence_leftUp_y, int fence_rightDown_x, int fence_rightDown_y, int objCenter_x, int objCenter_y){
    int judgeValue;
    /*需要中值滤波算法筛选得到新的稳定值*/
    if((objCenter_x >= fence_leftUp_x) && (objCenter_x <= fence_rightDown_x) && (objCenter_y >= fence_leftUp_y) && (objCenter_y <= fence_rightDown_y)){
        judgeValue = 0;
    }
    else
        judgeValue = 1; 
    return judgeValue;
}

/*int fenceSampleTest(int fence_leftUp_x, int fence_leftUp_y, int fence_rightDown_x, int fence_rightDown_y, RectBox headBox, int objCenter_y, int case_head, int case_body){
    if(case_body){

    }
}*/