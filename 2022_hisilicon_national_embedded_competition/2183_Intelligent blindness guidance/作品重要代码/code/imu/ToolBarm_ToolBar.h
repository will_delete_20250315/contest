#if !defined(AFX_TOOLBARM_TOOLBAR_H__D7C23800_AC10_4909_A50C_93E5D9F307CE__INCLUDED_)
#define AFX_TOOLBARM_TOOLBAR_H__D7C23800_AC10_4909_A50C_93E5D9F307CE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ToolBarm_ToolBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CToolBarm_ToolBar window

class CToolBarm_ToolBar : public CToolBarCtrl
{
	// Construction
public:
	CToolBarm_ToolBar();

	// Attributes
public:

	// Operations
public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CToolBarm_ToolBar)
		//}}AFX_VIRTUAL

	// Implementation
public:
	virtual ~CToolBarm_ToolBar();

	// Generated message map functions
protected:
	//{{AFX_MSG(CToolBarm_ToolBar)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TOOLBARM_TOOLBAR_H__D7C23800_AC10_4909_A50C_93E5D9F307CE__INCLUDED_)
