#include "stdafx.h"
#include "unpack.h"
#include "stdio.h"
#include "afxwin.h"
#include "navigation.h"
#include "math.h"
#include "acce_cali.h"
//****************变量定义*********************//

///////全局变量定义////////
extern int roll_front_idx;/*环形缓冲区前指针*/
extern int roll_back_idx;/*环形缓冲区后指针*/
extern unsigned long  roll_front;/*环形缓冲区前指针（为了解决零界归零问题）*/
extern unsigned long  roll_back;/*环形缓冲区后指针（为了解决零界归零问题）*/
extern unsigned char  roll_buffer[2048];/*环形缓冲区*/
extern FILE* imu_file;//惯性器件解包数据输出文件

extern int func_flag;//通过该标志位可以判断当前处于哪种状态，从而进行不同的操作
//对准状态的话进行数据的预处理--累加求平均，求G
//导航状态的话进行导航求解姿态速度位置。。。。


extern double modi_g;//引用外部变量使得在unpack中对MODI_G进行修改
/////////////////////////////////////////////
//////////该部分的值可以extern在ped_navidlg.cpp中，每次按下对准按钮之后都可以归零重新对准~~~
double gyro_drift_x = 0;
double gyro_drift_y = 0;
double gyro_drift_z = 0;//对准时刻陀螺的零偏的累加
double gyrodrift_x;
double gyrodrift_y;
double gyrodrift_z;//导航开始之前陀螺的零偏累加之后的求解
double acce_modi_x = 0;
double acce_modi_y = 0;
double acce_modi_z = 0;//对准时刻加表的零偏--每个每个--用于求解真实的g
double acce_drift_x = 0;
double acce_drift_y = 0;
double acce_drift_z = 0;//对准时刻陀螺的零偏的累加
double accedrift_x = 0;
double accedrift_y = 0;
double accedrift_z = 0;//对准时刻陀螺的零偏的累加之后的求解

double acce_modi = 0;
int drift_count = 0;//计算初始对准时候的值的个数

////////////////////////////////////////


/////////////////////来自acce_cali.cpp的全局变量/////////////////////////////
extern double acce_basis[3];//将加表的零偏定义为全局变量方便导航解算
//////////////////////////////////////////////////////////////////////////
//////////////////////定义在本cpp文件中的全局变量，用于加速度计在线标定////////////
double acce_db_g2[500];
double acce_db_x2[500];
double acce_db_y2[500];
double acce_db_z2[500];//设置为500个，10ms的情况下为5s，最多5s
int abdcount2 = 0;
double acce_db_g3[500];
double acce_db_x3[500];
double acce_db_y3[500];
double acce_db_z3[500];//设置为500个，10ms的情况下为5s，最多5s
int abdcount3 = 0;
double acce_db_g1[500];
double acce_db_x1[500];
double acce_db_y1[500];
double acce_db_z1[500];//设置为500个，10ms的情况下为5s，最多5s
int abdcount1 = 0;
////////////////////////////////////////////////////////////////////





///数据包结构体的定义
typedef struct {
	int samp_time;
	short acce[3];
	short gyro[3];
	short h[3];
	short alt;
	short tempr;
	short press;
}PACK_STRUCT;//根据数据包的结构定义结构体
//解包共用体的定义
typedef union {
	PACK_STRUCT pack;
	unsigned char byte[28];
}PACK_UNION;//为了解包定义的共同体

PACK_UNION pack_union;//解包共用体

int count_num = 0;//数据包包长的变量
int kc_count2 = 0;//用于计数的变量
int kc_count = 0;//用于计数的变量
int imu_count = 0;//用于计数的变量
unsigned char buffc;//利用getchar函数得到的int数据通过本变量转换为unsigned char变量
unsigned char buff[40] = { 0,0,0,0 };//数据包读取缓冲区
unsigned char pack_test[40] = { 0 };//数据包解包缓冲区（存放经过校验之后的完整数据包数据，由buff传入）
unsigned char bytec[30] = { 0 };//改变次序时使用的缓冲区
unsigned char check_sum = 0;//校验和

short temp = 0;//解包完成后取出数据过程中的数据中转//两个字节int为4个字节，long一般也为4个字节，64位机器中为64个字节
float imu_msg[12];//输出数据缓冲区
int samp_time;//输出时间，与数据一起输出


DWORD out_raw_number = 0;








//****************函数定义*********************///

//*************************************
//函数名：change2()
//将数据读取至缓冲区中并改变高低位顺序（一个数据两个字节）
//输入变量：读入的数组名称，数据个数；输出变量：读入的数组名称（通过形参进行参数传递）
//****************************************
void change2(unsigned char data_change[], int change_num)//共用体结构中byte数据次序调转函数
{
	int change_num1 = 0;
	unsigned char change_byte;
	for (change_num1 = 0; change_num1 < change_num; change_num1++)
	{
		change_byte = data_change[change_num1];
		data_change[change_num1] = data_change[change_num1 + 1];
		data_change[change_num1 + 1] = change_byte;
		change_num1 = change_num1 + 1;//不可以加2，因为for循环中还有加1
	}

}


//*************************************
//函数名：unpack_data()
//从缓冲区中读取数据并进行解包输出
//输入变量：无；输出变量：无
//****************************************
void unpack_data()
{





	while ((roll_back + 40) < roll_front)//判断环形缓冲区中是否存在一定的数据量，足够接一包数据
	{
		roll_back_idx = roll_back % 2048;
		buffc = roll_buffer[roll_back_idx];//读取数据缓冲区中的数据
		roll_back++;
		buff[0] = buff[1];
		buff[1] = buffc;
		if (buff[0] == 0XA5 && buff[1] == 0X5A)//是否为包头
		{
			roll_back_idx = roll_back % 2048;
			buffc = roll_buffer[roll_back_idx];
			roll_back++;
			buff[2] = buffc;
			count_num = buff[2];
			if (count_num == 0X20)//判断包的字节长度是否正确（防止包中含有A55A，误认包头）
			{
				for (kc_count2 = 0; kc_count2 < count_num - 1; kc_count2++)	//读取接下来一包的数据	
				{
					roll_back_idx = roll_back % 2048;
					buffc = roll_buffer[roll_back_idx];
					roll_back++;
					buff[kc_count2 + 3] = buffc;
				}
				check_sum = 0;	 //将上次一的校验和进行清零
				for (kc_count2 = 0; kc_count2 < count_num - 2; kc_count2++)
					check_sum = check_sum + buff[kc_count2 + 2];//进行累加和以便于下一步进行和校验



				if (check_sum == buff[count_num])//校验字节是否正确
				{
					for (kc_count = 0; kc_count < count_num + 2; kc_count++)
						pack_test[kc_count] = buff[kc_count];//将一整包数据完全放入新的数组pack_test中

					if (pack_test[3] == 0XA4)//判断当前是否为A3数据包
					{
						for (kc_count = 0; kc_count < 28; kc_count++)

						{
							pack_union.byte[kc_count] = pack_test[kc_count + 4];
							bytec[kc_count] = pack_union.byte[kc_count];
						}

						change2(bytec, 28);	//调用函数change2将数据的高地位进行转换，便于解包

						for (imu_count = 0; imu_count < 28; imu_count++)
						{
							pack_union.byte[imu_count] = bytec[imu_count];//将转换后的数据重新传给共用体
						}

						samp_time = bytec[1];
						samp_time = (samp_time << 8) + bytec[0];
						samp_time = (samp_time << 8) + bytec[3];
						samp_time = (samp_time << 8) + bytec[2];/////以上四步，将simp_time解包



					/*************************以下进行使用共用体进行数据解包的过程*************************/
						for (imu_count = 0; imu_count < 3; imu_count++)
						{
							temp = pack_union.pack.acce[imu_count];//加表解包数据负转换

							if (temp & 0x8000)
							{
								temp = 0 - (temp & 0x7fff);
							}
							else temp = (temp & 0x7fff);
							pack_union.pack.acce[imu_count] = temp;
						}
						for (imu_count = 0; imu_count < 3; imu_count++)
						{
							temp = pack_union.pack.gyro[imu_count];//陀螺解包数据负转换

							if (temp & 0x8000)
							{
								temp = 0 - (temp & 0x7fff);
							}
							else temp = (temp & 0x7fff);
							pack_union.pack.gyro[imu_count] = temp;
						}
						for (imu_count = 0; imu_count < 3; imu_count++)
						{
							temp = pack_union.pack.h[imu_count];//磁传感器解包数据负转换

							if (temp & 0x8000)
							{
								temp = 0 - (temp & 0x7fff);
							}
							else temp = (temp & 0x7fff);
							pack_union.pack.h[imu_count] = temp;
						}

						temp = pack_union.pack.alt;//气压高度
						if (temp & 0x8000)
						{
							temp = 0 - (temp & 0x7fff);
						}
						else temp = (temp & 0x7fff);
						pack_union.pack.alt = temp;

						temp = pack_union.pack.tempr;//摄氏度
						if (temp & 0x8000)
						{
							temp = 0 - (temp & 0x7fff);
						}
						else temp = (temp & 0x7fff);
						pack_union.pack.tempr = temp;

						temp = pack_union.pack.press;//气压压力
						if (temp & 0x8000)
						{
							temp = 0 - (temp & 0x7fff);
						}
						else temp = (temp & 0x7fff);
						pack_union.pack.press = temp;
						/*************************以上为进行使用共用体进行数据解包的过程*************************/

						imu_msg[0] = (float)pack_union.pack.acce[0] / 8192.0f;//分别使用各自的转换当量进行DA转换
						imu_msg[1] = (float)pack_union.pack.acce[1] / 8192.0f;
						imu_msg[2] = (float)pack_union.pack.acce[2] / 8192.0f;
						imu_msg[3] = (float)pack_union.pack.gyro[0] / 16.4f;
						imu_msg[4] = (float)pack_union.pack.gyro[1] / 16.4f;
						imu_msg[5] = (float)pack_union.pack.gyro[2] / 16.4f;
						imu_msg[6] = (float)pack_union.pack.h[0] * 0.92f;
						imu_msg[7] = (float)pack_union.pack.h[1] * 0.92f;
						imu_msg[8] = (float)pack_union.pack.h[2] * 0.92f;
						imu_msg[9] = (float)pack_union.pack.alt / 10.0f;
						imu_msg[10] = (float)pack_union.pack.tempr / 10.0f;
						imu_msg[11] = (float)pack_union.pack.press * 10.0f;

					}




					out_raw_number++;
					fprintf(imu_file, "%ld\t %d\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\t %f\n",
						out_raw_number, samp_time,
						imu_msg[0], imu_msg[1], imu_msg[2],
						imu_msg[3], imu_msg[4], imu_msg[5],
						imu_msg[6], imu_msg[7], imu_msg[8],
						imu_msg[9], imu_msg[10], imu_msg[11]); //将器件信息输出至文件

					fflush(imu_file);//刷新缓冲区

					buff[0] = 0;
					buff[1] = 0;

					///////////////////////////////////////////解完一包数据之后进行处理了需要/////////////////////////////
					//////////////////////////////////////////一下为根据标志位的不同判断不同的处理方法//////////////
					if (func_flag == 1)//对准状态
					{

						gyro_drift_x = imu_msg[3] + gyro_drift_x;
						gyro_drift_y = imu_msg[4] + gyro_drift_y;
						gyro_drift_z = imu_msg[5] + gyro_drift_z;//imu_msg的前三个为加表，后三个为陀螺，然后以此为磁感。。。。
						drift_count = drift_count + 1;
						acce_modi_x = imu_msg[0];
						acce_modi_y = imu_msg[1];
						acce_modi_z = imu_msg[2];
						acce_modi = acce_modi + sqrt(acce_modi_x * acce_modi_x + acce_modi_y * acce_modi_y + acce_modi_z * acce_modi_z);
						acce_drift_x = imu_msg[0] + acce_drift_x;
						acce_drift_y = imu_msg[1] + acce_drift_y;
						acce_drift_z = imu_msg[2] + acce_drift_z;

					}
					if (func_flag == 2)//导航状态
					{
						double imu_tmp_msg[14];//定义一个中转将所有的数据全部（包括采样时间和序号）输出至导航解算
						int i_count;//定义计数
						for (i_count = 0; i_count < 12; i_count++)
						{
							imu_tmp_msg[i_count + 2] = (double)imu_msg[i_count];
						}


						imu_tmp_msg[5] = imu_tmp_msg[5] - gyrodrift_x;
						imu_tmp_msg[6] = imu_tmp_msg[6] - gyrodrift_y;
						imu_tmp_msg[7] = imu_tmp_msg[7] - gyrodrift_z;//陀螺减去偏置

					//	imu_tmp_msg[5]=imu_tmp_msg[5];
					//	imu_tmp_msg[6]=imu_tmp_msg[6];
					//	imu_tmp_msg[7]=imu_tmp_msg[7];//陀螺减去偏置

					  //  imu_tmp_msg[2]=imu_tmp_msg[2];
						//imu_tmp_msg[3]=imu_tmp_msg[3];
						//imu_tmp_msg[4]=imu_tmp_msg[4];

						imu_tmp_msg[2] = imu_tmp_msg[2] - acce_basis[0];
						imu_tmp_msg[3] = imu_tmp_msg[3] - acce_basis[1];
						imu_tmp_msg[4] = imu_tmp_msg[4] - acce_basis[2];//加表减去偏置test_g2


						imu_tmp_msg[0] = (double)out_raw_number;
						imu_tmp_msg[1] = (double)samp_time;
						PedNavi(imu_tmp_msg);

					}

					if (func_flag == 3)//加速度计在线标定状态
					{

						acce_db_x1[abdcount1] = imu_msg[0];
						acce_db_y1[abdcount1] = imu_msg[1];
						acce_db_z1[abdcount1] = imu_msg[2];
						(abdcount1)++;//将计数值加一

					}
					if (func_flag == 4)//加速度计在线标定状态
					{

						acce_db_x2[abdcount2] = imu_msg[0];
						acce_db_y2[abdcount2] = imu_msg[1];
						acce_db_z2[abdcount2] = imu_msg[2];
						(abdcount2)++;//将计数值加一

					}
					if (func_flag == 5)//加速度计在线标定状态
					{

						acce_db_x3[abdcount3] = imu_msg[0];
						acce_db_y3[abdcount3] = imu_msg[1];
						acce_db_z3[abdcount3] = imu_msg[2];
						(abdcount3)++;//将计数值加一

					}





				}//校验字节是否正确	
			}//字长是否正确	
		}//是否是包头

	}//while
}