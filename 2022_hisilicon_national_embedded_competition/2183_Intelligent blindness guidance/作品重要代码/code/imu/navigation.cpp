#include "stdafx.h"
#include "navigation.h"
#include "math.h"
#include "align.h"-
#include "stdio.h"
#include "matrix.h"
#include "afxwin.h"
#include "hmr_yaw.h"
//////////////////////////测试变量定义///////////////////////////////
//FILE *fn_test;//测试FN的输出为何会有问题；fn1

///////////////////函数定义////////////////////////
 void AttiCal(double *imumsg,double *attimsg);//姿态解算函数
 void AngelUpdate(double  *wnbb);//四元数更新函数，服务于AttiCal
 void VeloCal(double *imumsg,double *attimsg,double *velomsg,double *posimsg);//速度解算函数
/* void PosiCal();//位置解算函数*///与速度解算函数合并了
 
extern FILE *navi_file;//文件的定义都都已在ped_navidlg.cpp中


///////////////////变量定义////////////////////////
double  pi=3.1415926;
double deg_rad=pi/180.0f;
double atti[3];
double velo[3];
double posi[3];
double t_samp=0.005;//采样解算时间//about time  6-19
double wnbbA0=0.0;//不可交换误差的公共变量
double WnbbX[4][4];//
double real_g=9.7803698;
double modi_g;
///////////////////////////////////////////////////







/////////////////////////////////////////////////
/////////解算函数（总函数）
////////作用：根据得到的新数据进行速度位置等解算
////////输入变量：传感器信息、位置信息
///////////////////////////////////////////////


//void PedNavi(double *msg)//获取解包数据进行导航
//{   
//   
//   
//
//   int i;
//   static long int out_number=0;//输出数据计数；
//   static double zupt_acce_msg[20]={0};//该变量为20个时刻的加表三轴输出的平方和开根，用于判断是否静止
//   double  zupt_check=0;
//   double zupt_check_g=0;//2013-04-26'by wanjunwei 
//   ///////////////////////////// 以下静态变量定义用作零速时刻的修正//////////////////////
//	//static double posiz[3];
//    static double attiz[3]={0};//零速时刻姿态的存放，便于与下一个零速时刻进行比较（初值的设置问题--初始值需设成和posi一致）
//	//static double veloz[3];
//	double attichange=0;//航向角的该变量
//
//	static double msg_acce[3][20];//建立一个数组存放前时刻的数据 
//           double msgacce[3];//存放平均值
//
//    static int first_z_flag=1;//该标志位用来判断是否是第一次静茹零速时刻，一次静止可能有多次进入零速时刻 2013-05-09 bywanjunwei
//
//   /////////////////////////////////////以上///////////////////////////////////////////
//	
//	
//	//////////////////以下操作将最新的值赋给静态数组的最后，然后进行判断//////////////////////////////
//	  for(i=0;i<19;i++)
//	  {
//	  zupt_acce_msg[i]=zupt_acce_msg[i+1];
//      
//	  msg_acce[0][i]=  msg_acce[0][i+1];
//	  msg_acce[1][i]=  msg_acce[1][i+1];
//	  msg_acce[2][i]=  msg_acce[2][i+1];//将数据队列更新
//
//	  }
//  	  zupt_acce_msg[19]=sqrt(msg[2]*msg[2]+msg[3]*msg[3]+msg[4]*msg[4]);
//      
//	  msg_acce[0][19]=msg[2];
//	  msg_acce[1][19]=msg[3];
//	  msg_acce[2][19]=msg[4];
//
//
//
///////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////以下判断最大值是否超限----以确定是否静止/////////////////////////////   
//	  for(i=0;i<20;i++)
//	{
//	  if (zupt_acce_msg[i]>zupt_check) 
//	  {
//			zupt_check=zupt_acce_msg[i];// 该处需要修改，-g。目前不修改，为了调试后面的程序//
//	  }
//	}
//   
//      
//
//	  zupt_check_g=fabs(zupt_check*modi_g-real_g);//2013-04-26 by wanjunwei----zupt-check-增加fabs-2013-05-09
//	  
//	  zupt_check=0;//将zupt——check重置为零  2013-05-09 by wanjunwei
//     
//	  if(zupt_check_g<0.2)//可以判断静止时刻，然后进行静止时刻的操作test_g3
//	{
//
//
//		  msgacce[0]=0;msgacce[1]=0;msgacce[2]=0;//使用前初始化
//	     ///////////////////////////////采用多个数据平均进行对准计算////////////////////////////
//          for(i=0;i<10;i++)
//		  {
//			  msgacce[0]= msgacce[0]+msg_acce[0][i+5];
//			  msgacce[1]= msgacce[1]+msg_acce[1][i+5];
//			  msgacce[2]= msgacce[2]+msg_acce[2][i+5];//仅取稳态较高的值
//		  }
//          
//		  msgacce[0]=msgacce[0]/10;
//		  msgacce[1]=msgacce[1]/10;
//		  msgacce[2]=msgacce[2]/10;
//
//		  AlignAtti(msgacce[0],msgacce[1],msgacce[2]);//静止时刻使用加表进行重新姿态角定位(横滚，俯仰)
//          AlignVelo();//静止时刻将速度都设置为零
//          //////////////////////航向角的修正(以下)////////////////////////////////
//		  //////////////////////////第一次进入零速修正时刻对航向角修正/////////////////////////	
//		  if (first_z_flag==1) 
//		  {
//		  
//		  attichange=fabs(attiz[2]-atti[2]);
//		  if (attichange>180)//零界情况
//		  {
//		     if (attiz[2]<atti[2]) 
//			 {
//		     attichange=fabs(attiz[2]+360.0-atti[2]);//from 2 to 358
//			 }
//			 if (attiz[2]>atti[2]) 
//			 {
//		     attichange=fabs(attiz[2]-360.0-atti[2]);//from 358 to 2
//			 }
//		  }
//		  
//		  if (attichange<5)//若两个差值小于5, 则拉回来
//			{
//			 //atti[2]=attiz[2];
//			}
//            attiz[2]=atti[2];//将此时刻的值赋给下一时刻使用。
//		  //////////////////////航向角的修正(以上)////////////////////////////////
//		  first_z_flag=0;
//		  }
//		  
//	}
//      else//如果没有静止，则直接进行正常导航
//	{
//	   AttiCal(msg,atti);
//	   VeloCal(msg,atti,velo,posi);
//      first_z_flag=1;
//	 
//   	}
///////////////////////////////////////////////////////////////////////////////////////////
// 
//	out_number++;
//   fprintf(navi_file,"%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",
//	 	  out_number,msg[1],
//		  atti[0],atti[1],atti[2],
// 	      velo[0],velo[1],velo[2],
//  	       posi[0],posi[1],posi[2]);
//  fflush(navi_file);
//
//}


/////////////////////////////////////////////////
/////////姿态解算函数
////////作用：根据得到的新数据进行姿态解算
////////输入变量：传感器信息、姿态信息（通过形参传递更新姿态角）
///////////////////////////////////////////////
void AttiCal(double *imumsg,double *attimsg)
{
 
	
 double attiN[3];
 double Cbn[3][3];
 double q[4],qb[4];
 double q_norm;//规范化四元数
 double cc[3], ss[3];
 double wnbb[3];
 int k;
 double c_q,d_q;
 attiN[0]=attimsg[0]*pi/180.0;// 横滚
 attiN[1]=attimsg[1]*pi/180.0;// 俯仰
 attiN[2]=attimsg[2]*pi/180.0;// 航向


 ////////////姿态转换矩阵/////////从N到B/////////(已检查2013-04-17 wjw)	
 for( k=0; k<3; k++ )   //计算sin cos值，方便下面计算
 {
	ss[k]=sin(attiN[k]);
	if( attiN[k] == 90.0*deg_rad ) cc[k]=0.0; // 避免计算误差
	else   cc[k]=cos(attiN[k]);
 }	
  
    Cbn[0][0]=cc[0]*cc[2]+ss[0]*ss[1]*ss[2];
	Cbn[1][0]=cc[1]*ss[2];
	Cbn[2][0]=ss[0]*cc[2]-cc[0]*ss[1]*ss[2];
	Cbn[0][1]=-cc[0]*ss[2]+ss[0]*ss[1]*cc[2];
	Cbn[1][1]=cc[1]*cc[2];
	Cbn[2][1]=-ss[0]*ss[2]-cc[0]*ss[1]*cc[2];
	Cbn[0][2]=-ss[0]*cc[1];
	Cbn[1][2]=ss[1];
	Cbn[2][2]=cc[0]*cc[1];
///////////////四元数求解///////////////////////////(已检查2013-04-17 wjw)/ 	
	for( k=0; k<3; k++ )    
	{
		ss[k]=sin(attiN[k]*0.5);
		if( attiN[k]*0.5 == 90.0*deg_rad ) cc[k]=0.0;//避免计算误差
        else  
			cc[k]=cos(attiN[k]*0.5);
	}

 	q[0]=cc[2]*cc[1]*cc[0]+ss[2]*ss[1]*ss[0];
	q[1]=cc[2]*ss[1]*cc[0]+ss[2]*cc[1]*ss[0];
	q[2]=cc[2]*cc[1]*ss[0]-ss[2]*ss[1]*cc[0];
	q[3]=-ss[2]*cc[1]*cc[0]+cc[2]*ss[1]*ss[0];

///////////////////求解新的四元数并修正///////////////////////////////////////////////
	
	wnbb[0]=imumsg[5]*pi/180.0 ;    //  单位：弧度/秒
	wnbb[1]=imumsg[6]*pi/180.0 ;    //  单位：弧度/秒
	wnbb[2]=imumsg[7]*pi/180.0 ;    //  单位：弧度/秒(暂未考虑wien等项目)

	AngelUpdate(wnbb);	//修正不可交换性误差（暂时还少CROSS的定义）


    c_q=cos( 0.5*wnbbA0 );///对应于matlab中的c_q///邢丽2012-03-22注释
	d_q=(wnbbA0==0.0)?  0.5:  (sin( 0.5*wnbbA0 ))/wnbbA0;///对应于matlab中的d_q///邢丽2012-03-22注释
	
	for( k=0;k<4;k++)
	{
		/*qb[k]=c_q*q[k]+d_q*(WnbbX[k][0]*q[0]+WnbbX[k][1]*q[1]+WnbbX[k][2]*q[2]+WnbbX[k][3]*q[3]);*/
		qb[k]=c_q*q[k]+d_q*(WnbbX[k][0]*q[0]+WnbbX[k][1]*q[1]+WnbbX[k][2]*q[2]+WnbbX[k][3]*q[3]);
	}
	

/////////// 四元素规范化 ////////////////////////////////////////	
	q_norm=sqrt(qb[0]*qb[0]+qb[1]*qb[1]+qb[2]*qb[2]+qb[3]*qb[3]);
	for( k=0; k<4; k++ )   q[k]=qb[k]/q_norm; //四元素规范化

///////////////四元数反解姿态矩阵///////////////////////////(已检查2013-04-17 wjw)/ 
  Cbn[0][0] = q[0]*q[0]+q[1]*q[1]-q[2]*q[2]-q[3]*q[3];
  Cbn[1][1] = q[2]*q[2]-q[3]*q[3]+q[0]*q[0]-q[1]*q[1];
  Cbn[2][2] = q[3]*q[3]-q[2]*q[2]-q[1]*q[1]+q[0]*q[0];
  Cbn[0][1] = 2.0e0 * (q[1]*q[2]+q[0]*q[3]);
  Cbn[0][2] = 2.0e0 * (q[1]*q[3]-q[0]*q[2]);
  Cbn[1][0] = 2.0e0 * (q[1]*q[2]-q[0]*q[3]);
  Cbn[1][2] = 2.0e0 * (q[2]*q[3]+q[0]*q[1]);
  Cbn[2][0] = 2.0e0 * (q[1]*q[3]+q[0]*q[2]);
  Cbn[2][1] = 2.0e0 * (q[2]*q[3]-q[0]*q[1]);
////////////////姿态矩阵反解姿态角///////////////////////////////	
  attiN[0]=atan(-Cbn[0][2]/Cbn[2][2]);
  attiN[1]=atan(Cbn[1][2]/sqrt(Cbn[1][0]*Cbn[1][0]+Cbn[1][1]*Cbn[1][1]));
  attiN[2]=atan(Cbn[1][0]/Cbn[1][1]);
	
  attiN[0]=attiN[0]*180.0/pi;
  attiN[1]=attiN[1]*180.0/pi;
  attiN[2]=attiN[2]*180.0/pi;	
 ////////////////象限判断//////////////////从matlab转化过来//////////////////// 
  
  if(Cbn[1][1]<0 ) 
  {
	  attiN[2]=180.0+attiN[2];
  }
  else 
  {
	if(Cbn[1][0]<0) attiN[2]=360.0+attiN[2]; 
  }
  
  if(Cbn[2][2]<0)  
  {
   if(Cbn[0][2]>0) attiN[0]=-(180.0-attiN[0]); 
   if(Cbn[0][2]<0) attiN[0]=(180.0+attiN[0]); 
  }
 	
   attimsg[0]=attiN[0];// 横滚
   attimsg[1]=attiN[1];// 俯仰
   attimsg[2]=attiN[2];// 航向（通过函数参数将更新后的姿态角传出去）
}



/////////////////////////////////////////////////
/////////子函数：四元数更新服务函数，服务于AttiCal
////////作用：修正不可交换性误差 ---得到如下变量方便于四元素计算
//float wnbbA0=0.0;//不可交换误差的公共变量
//float WnbbX[4][4];
////////输入变量：wnbb
///////////////////////////////////////////////

 void AngelUpdate(double *wnbb)
 {
   
	int k,j;
/*	double   wb_tmp[3], wb_tmp1[3];*/
	static double wnbbA[3]={ 0.0e0, 0.0e0, 0.0e0 },
	              wnbbA_old[3]={ 0.0e0, 0.0e0, 0.0e0 };
	static int kctrl_angup=0;
	static double	wnbbA_e[3];
    static double	wnbbX_e[4][4],
                    wnbbX[4][4];
	
	//added by chenjh 2010-12-29 每次程序运行，将三个局部静态变量复位=-----暂时没明白源程序的意思
//	if (ctrl_angleupdata == 1)
//	{
//		wnbbA[0] = 0.0e0;
//		wnbbA[1] = 0.0e0;
//		wnbbA[2] = 0.0e0;
//		
//		wnbbA_old[0] = 0.0e0;
//		wnbbA_old[1] = 0.0e0;
//		wnbbA_old[2] = 0.0e0;
//		kctrl_angup=0;
//		
//		ctrl_angleupdata = 0;
//	}
	
	for( k=0; k<3; k++ ) 
	{
		wnbbA[k] = t_samp*wnbb[k];///matlab中对应的WnbbA
	}
	 wnbbA0=sqrt(wnbbA[0]*wnbbA[0]+wnbbA[1]*wnbbA[1]+wnbbA[2]*wnbbA[2]);///matlab中对应的WnbbA0,这是需要传出的值
	////wnbbX对应于matlab中的WnbbX/////
     wnbbX[0][0]=0.0e0;      wnbbX[0][1]=-wnbbA[0];    wnbbX[0][2]=-wnbbA[1];     wnbbX[0][3]=-wnbbA[2];
     wnbbX[1][0]=wnbbA[0];   wnbbX[1][1]=0.0e0;        wnbbX[1][2]=wnbbA[2];      wnbbX[1][3]=-wnbbA[1];
	 wnbbX[2][0]=wnbbA[1];   wnbbX[2][1]=-wnbbA[2];    wnbbX[2][2]=0.0e0;         wnbbX[2][3]=wnbbA[0];
	 wnbbX[3][0]=wnbbA[2];   wnbbX[3][1]=wnbbA[1];     wnbbX[3][2]=-wnbbA[0];     wnbbX[3][3]=0.0e0;
	
    ///等效转动矢量修正算法///
	cross(wnbbA_old,wnbbA,wnbbA_e); ////对应matlab中的WnbbA_e的求取
    ///wnbbX_e对应于matlab中的WnbbX_e///
    wnbbX_e[0][0]=0.0e0;          wnbbX_e[0][1]=-wnbbA_e[0];     wnbbX_e[0][2]=-wnbbA_e[1];     wnbbX_e[0][3]=-wnbbA_e[2];     
    wnbbX_e[1][0]=wnbbA_e[0];     wnbbX_e[1][1]=0;               wnbbX_e[1][2]=wnbbA_e[2];      wnbbX_e[1][3]=-wnbbA_e[1];
	wnbbX_e[2][0]=wnbbA_e[1];     wnbbX_e[2][1]=-wnbbA_e[2];     wnbbX_e[2][2]=0.0e0;           wnbbX_e[2][3]=wnbbA_e[0];
    wnbbX_e[3][0]=wnbbA_e[2];     wnbbX_e[3][1]=wnbbA_e[1];      wnbbX_e[3][2]=-wnbbA_e[0];     wnbbX_e[3][3]=0.0e0;

	for( k=0;k<4;k++)
	{
		for( j=0;j<4;j++)
		{
		    WnbbX[k][j]=wnbbX[k][j]+1/12*wnbbX_e[k][j];
		}
	}
	
	for( k=0; k<3; k++ )  
	{
		wnbbA_old[k] =wnbbA[k];
	}
	
	return;

 }

 
 
/////////////////////////////////////////////////
/////////速度位置解算函数
////////作用：根据得到的新数据进行速度位置解算
////////输入变量：传感器信息、姿态信息、速度信息、位置信息（通过形参传递更新姿态角）
///////////////////////////////////////////////

 void VeloCal(double *imumsg,double *attimsg,double *velomsg,double *posimsg)//速度解算函数
 {
	 double attiN[3];
	 double Cbn[3][3];
	 double cc[3], ss[3];
     double fn[3];
	 int k;
	 

	 attiN[0]=attimsg[0]*pi/180.0;// 横滚
	 attiN[1]=attimsg[1]*pi/180.0;// 俯仰
	 attiN[2]=attimsg[2]*pi/180.0;// 航向


	 ////////////姿态转换矩阵/////////从N到B/////////(已检查2013-04-17 wjw)	
	 for( k=0; k<3; k++ )   //计算sin cos值，方便下面计算
	 {
		ss[k]=sin(attiN[k]);
		if( attiN[k] == 90.0*deg_rad ) cc[k]=0.0; // 避免计算误差
		else   cc[k]=cos(attiN[k]);
	 }	
  
	   Cbn[0][0]=cc[0]*cc[2]+ss[0]*ss[1]*ss[2];
	   Cbn[1][0]=cc[1]*ss[2];
	 Cbn[2][0]=ss[0]*cc[2]-cc[0]*ss[1]*ss[2];
	 Cbn[0][1]=-cc[0]*ss[2]+ss[0]*ss[1]*cc[2];
	    Cbn[1][1]=cc[1]*cc[2];
	Cbn[2][1]=-ss[0]*ss[2]-cc[0]*ss[1]*cc[2];
	Cbn[0][2]=-ss[0]*cc[1];
	Cbn[1][2]=ss[1];
	Cbn[2][2]=cc[0]*cc[1];

       fn[0]=imumsg[2]*Cbn[0][0]+imumsg[3]*Cbn[1][0]+imumsg[4]*Cbn[2][0];
       fn[1]=imumsg[2]*Cbn[0][1]+imumsg[3]*Cbn[1][1]+imumsg[4]*Cbn[2][1];
       fn[2]=imumsg[2]*Cbn[0][2]+imumsg[3]*Cbn[1][2]+imumsg[4]*Cbn[2][2];//转化加表输出

	  //  fn[0]=imumsg[2]*Cbn[0][0]+imumsg[3]*Cbn[0][1]+imumsg[4]*Cbn[0][2];
      // fn[1]=imumsg[2]*Cbn[1][0]+imumsg[3]*Cbn[1][1]+imumsg[4]*Cbn[1][2];
      // fn[2]=imumsg[2]*Cbn[2][0]+imumsg[3]*Cbn[2][1]+imumsg[4]*Cbn[2][2];//转化加表输出
	
 	   fn[0]=fn[0]*real_g;
	   fn[1]=fn[1]*real_g;
	   fn[2]=fn[2]*real_g-real_g;//求解真实加速度//g_test1//2013-05-14 修改G by wanjunwei2

    //  fprintf(fn_test,"%lf\t%lf\t%lf\t%lf\n",imumsg[1],fn[0],fn[1],fn[2]);//fn2

	

	   velomsg[0]= velomsg[0]+t_samp*fn[0];
	   velomsg[1]= velomsg[1]+t_samp*fn[1];
	   velomsg[2]= velomsg[2]+t_samp*fn[2];//进行速度更新
       posimsg[0]= posimsg[0]+t_samp*velomsg[0];
	   posimsg[1]= posimsg[1]+t_samp*velomsg[1];
	   posimsg[2]= posimsg[2]+t_samp*velomsg[2];//进行位置解算		
 }






 void PedNavi(double *msg)//获取解包数据进行导航
{   

/////////////////////////////////////////////////
/////////解算函数（总函数）
////////作用：根据得到的新数据进行速度位置等解算
////////输入变量：传感器信息、位置信息
///////////////////////////////////////////////
	 

    //static double posiz[3];
	//static double veloz[3];
	static int wind_num=0;
	int i,j;
    static long int out_number=0;//输出数据计数；
	static double wind_msg[70][14];//前向窗口缓冲区//about time  6-19
    double zupt_acce_msg[40]={0};//该变量为20个时刻的加表三轴输出的平方和开根，用于判断是否静止//about time  6-19
    double  zupt_check=0;
	double msgacce[3];//存放平均值:acce
	double msghmr[3];//存放平均值:hmr
	double zupt_check_g=0;//2013-04-26'by wanjunwei 
	
	static double attiz[3]={0,0,0};//零速时刻姿态的存放，便于与下一个零速时刻进行比较（初值的设置问题--初始值需设成和posi一致）
    static double attihmrz=0;//零速时刻姿态的存放，该姿态值由磁感提供
	
	double attichange=0;//航向角的该变量
    static int first_z_flag=1;//该标志位用来判断是否是第一次静茹零速时刻，一次静止可能有多次进入零速时刻 2013-05-09 by wanjunwei
    double fwin_msg[14];//若窗口为前向窗口，该数组用来存放前向窗口的第一个数据。
	
	for (i=0;i<14;i++)
	{
		wind_msg[wind_num][i]=msg[i];
       
	}//来一个点将数据存入缓冲区进行处理//对于wind_num的全局变量也可以放在这里
       wind_num++;
	
	
	
	if (wind_num>40)//about time  6-19
	 {
		
		for(i=1;i<40;i++)//about time  6-19
		{
		  zupt_acce_msg[i]=sqrt(wind_msg[i][2]*wind_msg[i][2]+wind_msg[i][3]*wind_msg[i][3]+wind_msg[i][4]*wind_msg[i][4]);//往后20个时刻的加表的总值 
		}
		///////////////////判断最大值/////////////////////////////
		for(i=1;i<40;i++)//about time  6-19
		{
		  if (fabs(zupt_acce_msg[i])>fabs(zupt_check)) //6-19
		  {
				zupt_check=zupt_acce_msg[i];// 该处需要修改，-g。目前不修改，为了调试后面的程序//
		  }
		}
	   zupt_check_g=fabs(zupt_check*real_g-real_g);
       zupt_check=0;//将zupt——check重置为零,为了下一时刻的判断  2013-05-09 by wanjunwei

////////////////////////////////以上为使用加表的零速判断/////////////////////////////////////////////////////
		double w_max,w_min;
		w_max=wind_msg[0][7];
		w_min=wind_msg[0][7];
		static int davu_w_flag=0;

		for (i=1;i<20;i++)//about time  6-19
		{
		if (wind_msg[i][7]>w_max)
		{
			w_max=wind_msg[i][7];
		}
		if (wind_msg[i][7]<w_min)
		{
			w_min=wind_msg[i][7];
		}
		}

		if (fabs(w_min-w_max)<3)//about time  6-19
		{
		    davu_w_flag=1;
		}




		if(zupt_check_g<0.2)//2013-06-25修改零速条件
	//	if(davu_w_flag==1)
		{
        // davu_w_flag=0;
         AlignVelo();//静止时刻将速度都设置为零
		}
/////////////////////////////////以上为使用陀螺的零速判断///////////////////////////////////////////////




        if(davu_w_flag==1)
	    //if(zupt_check_g<0.3)//可以判断静止时刻，然后进行静止时刻的操作test_g3//about time  6-19
		{
            davu_w_flag=0;
		    msgacce[0]=0;msgacce[1]=0;msgacce[2]=0;//使用前初始化
			msghmr[0]=0;msghmr[1]=0;msghmr[2]=0;//使用前初始化
	     ///////////////////////////////采用多个数据平均进行对准计算////////////////////////////
          for(i=0;i<20;i++)//about time  6-19
		  {
			  msgacce[0]= msgacce[0]+wind_msg[i][2];
			  msgacce[1]= msgacce[1]+wind_msg[i][3];
			  msgacce[2]= msgacce[2]+wind_msg[i][4];//plus of acce
		  }
		    for(i=0;i<20;i++)//about time  6-19
		  {
			  msghmr[0]= msghmr[0]+wind_msg[i][8];
			  msghmr[1]= msghmr[1]+wind_msg[i][9];
			  msghmr[2]= msghmr[2]+wind_msg[i][10];//plus of hmr
		  }


          
		  msgacce[0]=msgacce[0]/20;//about time  6-19
		  msgacce[1]=msgacce[1]/20;
		  msgacce[2]=msgacce[2]/20;//mean of acce

          msghmr[0]=msghmr[0]/20;
		  msghmr[1]=msghmr[1]/20;
		  msghmr[2]=msghmr[2]/20;//mean of hmr

		  AlignAtti(msgacce[0],msgacce[1],msgacce[2]);//静止时刻使用加表进行重新姿态角定位(横滚，俯仰)
          AlignVelo();//静止时刻将速度都设置为零
		
		  /////////////////////////第一次进入零速修正时刻修正航向角/////////////////后面可以选择优化////////////////////////////////
		  if (first_z_flag==2) //	//2013-06-25暂时屏蔽
		  {
		  double attichg_hmr;
		  static double hmryaw[1]; 
		  attichange=atti[2]-attiz[2];
		  
		  		  if (fabs(attichange)>180)//零界情况
		  {
			 if (atti[2]>attiz[2]) 
				 {
				  attichange=atti[2]-360.0-attiz[2];//from 358 to 2
				 }
				 if (attiz[2]>atti[2]) 
				 {
				  attichange=atti[2]+360.0-attiz[2];//from 2 to 358
				 }
		  }
			
		  hmr_yaw(atti,msghmr,hmryaw);
		  
		  attichg_hmr=hmryaw[0]-attihmrz;
          
		  if (fabs(attichg_hmr)>180)//零界情况
		  {
			 if (attihmrz<hmryaw[0]) 
				 {
					attichange=hmryaw[0]-360.0-attihmrz;//from 358 to 2
				}
				 if (attihmrz>hmryaw[0]) 
				 {
				  attichange=hmryaw[0]+360.0-attihmrz;//from 2 to 358
				 }
		  }
		  
		  
 		  if ( fabs(attichange)>(2*fabs(attichg_hmr)))
		  {
          atti[2]=attiz[2]+attichg_hmr;//如果航向角的变化不大，则以航向角的变化为准
         }
           attihmrz=hmryaw[0];
            attiz[2]=atti[2];//将此时刻的值赋给下一时刻使用。
		  //////////////////////航向角的修正(以上)////////////////////////////////
		  first_z_flag=0;
		  }
		
	}
	
			else//如果没有静止，则直接进行正常导航
			{
			  
				for(i=0;i<14;i++)//about time  6-19??  14还是20
				{
				fwin_msg[i]=wind_msg[0][i];
				}
			   AttiCal(fwin_msg,atti);//换窗口之后不能直接使用MSG了，需要使用最前面的数据、可以另外开辟一个数组，fwin_msg
			   VeloCal(fwin_msg,atti,velo,posi);		
			   // AttiCal(msg,atti);//换窗口之后不能直接使用MSG了，需要使用最前面的数据、可以另外开辟一个数组，fwin_msg
			   // VeloCal(msg,atti,velo,posi);
			  first_z_flag=1;
			 
   			}
				
			   out_number++;
			   fprintf(navi_file,"%ld\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",
	 				  out_number,msg[1],
					  atti[0],atti[1],atti[2],
 					  velo[0],velo[1],velo[2],
  					   posi[0],posi[1],posi[2]);
			  fflush(navi_file);	

		
			  for(i=1;i<wind_num;i++)	
			  {
                  for(j=0;j<14;j++)	
				  {
				  wind_msg[i-1][j]= wind_msg[i][j];//将数组前向移位1位
				  }
					
			  }
	
	             wind_num=wind_num-1;
	
	}

}