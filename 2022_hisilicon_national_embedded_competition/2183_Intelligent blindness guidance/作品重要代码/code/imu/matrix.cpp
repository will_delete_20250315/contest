#include "stdafx.h"
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include "matrix.h"

void cross( double *para1, double *para2, double *para3)
///////////////////////////////////////////////////////////////////////////////
// cross:  两个三维向量的叉乘运算，实现para3=para1叉乘para2
// 设para1[3]={a1,b1,c1}  para2[3]={a2,b2,c2} para3[3]={b1c2-b2c1,c1a2-a1c2,a1b2-a2b1}
//
//
//                               南京航空航天大学导航研究中心编制 2012年03月 xingli
///////////////////////////////////////////////////////////////////////////////
{
    para3[0]=para1[1]*para2[2]-para1[2]*para2[1];
    para3[1]=para1[2]*para2[0]-para1[0]*para2[2];
    para3[2]=para1[0]*para2[1]-para1[1]*para2[0];
}
